import { FOOD_INTAKE, FOOD_TYPE, ITEM } from "./SliceName";

export const FETCH_ITEM_PREFIX = ITEM + "/fetchItems";
export const FETCH_FOOD_TYPE_PREFIX = FOOD_TYPE + "/fetchTypes";
export const FETCH_FOOD_INTAKE_PREFIX = FOOD_INTAKE + "/fetchIntake";
export const SAVE_FOOD_INTAKE_PREFIX = FOOD_INTAKE + "/saveIntake";
