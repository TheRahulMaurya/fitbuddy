export const ITEM = "item";
export const SELECT_FOOD = "selectFood";
export const FOOD_CART = "foodCart";
export const SELECT_FOOD_TYPE = "selectFoodType";
export const FOOD_TYPE = "type";
export const FOOD_INTAKE = "intake";
export const JWT = "jwt";
