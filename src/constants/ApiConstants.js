export const FITBUDDY_ROOT_END_POINT = "http://192.168.0.105:9006/api/v1";
export const LOGIN_END_POINT = "/login";
export const FETCH_ITEMS_END_POINT = "/items";
export const FETCH_FOOD_TYPES_END_POINT = "/foods/meals";
export const FETCH_USER_FOOD_INTAKE_FOR_DAY_END_POINT = "/intakes/users/";
export const FOOD_INTAKE_ROOT_END_POINT = "/intakes";
export const FETCH_RECOMMENDED_FOOD_END_POINT = "/foods";
export const FETCH_ALL_FOOD_END_POINT = "/foods";
export const AUTH_SERVICE_END_POINT = FITBUDDY_ROOT_END_POINT + "/auth";
export const SEND_OTP_END_POINT = "/send-mail-otp";
export const VERIFY_OTP_END_POINT = "/verify-otp";
export const FORGOT_PASSWORD_END_POINT = "/forgot-password";
export const USER_LOGIN_END_POINT = "/login";
export const USER_REGISTRATION_END_POINT = "/register";
export const GET_ALL_USERS_END_POINT = "/users";
export const GET_UNIQUE_USER_END_POINT = "/users/";
export const USER_WATER_INTAKE_FOR_A_DAY_END_POINT="/water";

