import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import {
  AntDesign,
  FontAwesome5,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import { useTailwind } from "tailwind-rn/dist";

interface NavigationProps {
  navigation: any;
  route: any;
}
const WeightGoalScreen = ({ navigation, route }: NavigationProps) => {
  const { cw, tw, hcwc, htwc } = route.params;
  //console.log(cw);

  const [currWeight, setCurrWeight] = useState<Number>(cw);
  const [trWeight, setTrWeight] = useState<Number>(tw);

  const handleCurrentWeightChange = (weight: number) => {
    setCurrWeight(weight);
    hcwc(weight);
  };

  const handleTargetWeightChange = (weight: number) => {
    setTrWeight(weight);
    htwc(weight);
  };

  const tww = useTailwind();

  const handleCurrentEdit = () => {
    navigation.navigate("WeightPicker", {
      weight: currWeight,
      handleChange: handleCurrentWeightChange,
    });
  };
  const handleTargetEdit = () => {
    navigation.navigate("WeightPicker", {
      weight: trWeight,
      handleChange: handleTargetWeightChange,
    });
  };

  const onClose = () => {
    navigation.navigate("WeightTracker");
  };

  return (
    <View
      style={{
        ...tww("bg-orange-200 flex-auto flex-col-reverse h-full w-full mb-0"),
        backgroundColor: "#ACACAC",
      }}
    >
      <View style={tww("flex-col-reverse h-2/3")}>
        <View style={tww("w-full h-2/3 bg-white rounded-t-3xl")}>
          <Text style={tww("py-3 text-2xl font-semibold ml-3 mt-6")}>
            Edit Weights
          </Text>

          <View style={styles.goalContainer}>
            <FontAwesome5 name="weight" size={25} color="purple" />
            <View
              style={{
                padding: 5,
                backgroundColor: "#F5F5F5",
                borderRadius: 5,
                width: "50%",
                marginLeft: 15,
              }}
            >
              <Text>Current Weight </Text>
              <Text>{currWeight}</Text>
            </View>

            <View style={styles.editIcon}>
              <FontAwesome5
                solid
                name="edit"
                size={20}
                color="black"
                onPress={handleCurrentEdit}
              />
            </View>
          </View>
          <View style={styles.goalContainer}>
            <MaterialCommunityIcons
              name="bullseye-arrow"
              size={25}
              color="purple"
            />
            <View
              style={{
                padding: 5,
                backgroundColor: "#F5F5F5",
                borderRadius: 5,
                width: "50%",
                marginLeft: 15,
              }}
            >
              <Text>Target Weight </Text>
              <Text>{trWeight}</Text>
            </View>

            <View
              style={{
                flexDirection: "row-reverse",

                width: "40%",
              }}
            >
              <FontAwesome5
                solid
                name="edit"
                size={20}
                color="black"
                onPress={handleTargetEdit}
              />
            </View>
          </View>
        </View>
        <View style={tww("flex-row-reverse pb-4 ml-3")}>
          <AntDesign name="close" size={30} color="black" onPress={onClose} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  goalContainer: { flexDirection: "row", margin: 15 },

  editIcon: {
    flexDirection: "row-reverse",

    width: "40%",
  },
});
export default WeightGoalScreen;
