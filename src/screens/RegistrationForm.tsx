import React, { useState } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Platform,
  Alert,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import CustomButton from "../components/globals/CustomButton";
import axios from "axios";
import { TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { USER_REGISTRATION_END_POINT } from "../constants/ApiConstants";
import GenderInput from "../components/globals/Gender";
import { LOGIN_SCREEN } from "../constants/ScreenNames";
import CountryInput from "../components/globals/Country";
import StateInput from "../components/globals/State";
import CityInput from "../components/globals/City";
import { FitBuddyApi } from "../apis/FitBuddyApi";
interface NavigationProps {
  navigation: any;
}
interface Address {
  street: string;
  city: string;
  state: string;
  pincode: string;
  landmark: string;
  country: string;
}
interface UserWeight {
  wtCurrent: string;
  wtGoal: string;
}
interface UserCredential {
  email: string;
  username: string;
  password: string;
}
interface User {
  firstName: string;
  lastName: string;
  mobile: string;
  dob: Date | null;
  gender: string;
  addresses: Address[];
  userWeight: UserWeight;
  targetSteps: string;
  userCredential: UserCredential;
}
const RegistrationForm = ({ navigation }: NavigationProps) => {
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [mobile, setMobile] = useState<string>("");
  const [dob, setDob] = useState<Date | null>(null);
  const [gender, setGender] = useState<string>("");
  const [street, setStreet] = useState<string>("");
  const [landmark, setLandmark] = useState<string>("");
  const [city, setCity] = useState<string>("");
  const [state, setState] = useState<string>("");
  const [country, setCountry] = useState<string>("");
  const [pincode, setPincode] = useState<string>("");
  const [wtCurrent, setWtCurrent] = useState<string>("");
  const [wtGoal, setWtGoal] = useState<string>("");
  const [targetSteps, setTargetSteps] = useState<string>("");
  const [username, setUserName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [submitted, setSubmitted] = useState<boolean>(false);
  const [showDatePicker, setShowDatePicker] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const handleTogglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const handleDateOfBirthPress = () => {
    setShowDatePicker(true);
  };
  const handleDateOfBirthChange = (
    _event: any,
    selectedDate: Date | undefined
  ) => {
    const currentDate = selectedDate || dob;
    setShowDatePicker(Platform.OS === "ios");
    setDob(currentDate);
  };

  const passwordRegex =
    /^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$&*])(?=.*[0-9]).{8,}$/;
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  const handleSubmits = async () => {
    setSubmitted(true);
    if (
      mobile.length != 10 ||
      !/^[a-zA-Z]+$/.test(firstName) ||
      !landmark ||
      pincode.length != 6 ||
      !dob ||
      !passwordRegex.test(password) ||
      !emailRegex.test(email) ||
      !street ||
      !targetSteps ||
      !wtCurrent ||
      !wtGoal ||
      !gender ||
      !city ||
      !country ||
      !state ||
      !lastName ||
      !username
    ) {
      console.log("inside validation ");
      return;
    }
    console.log("outside validation ");
    const user: User = {
      firstName,
      lastName,
      mobile,
      dob,
      gender,
      addresses: [
        {
          street,
          city,
          state,
          pincode,
          landmark,
          country,
        },
      ],
      userWeight: {
        wtCurrent,
        wtGoal,
      },
      targetSteps,
      userCredential: {
        email,
        username,
        password,
      },
    };

    //Post Request
    await FitBuddyApi.post(USER_REGISTRATION_END_POINT, user)
      .then((response) => {
        setFirstName("");
        setLastName("");
        setEmail("");
        setGender("");
        setMobile("");
        setLandmark("");
        setStreet("");
        setCity("");
        setPincode("");
        setDob(null);
        setState("");
        setCountry("");
        setTargetSteps("");
        setWtCurrent("");
        setWtGoal("");
        setUserName("");
        setPassword("");
        if (response.status == 201) {
          console.log(
            "Successfully registered : \n" + JSON.stringify(response.data)
          );
          Alert.alert("Details added ", "You are successfully registered Now");
          navigation.navigate("VerifyOtp", { email: email });
        }
      })
      .catch((error) => {
        console.log(error);
        //handle 401 400 error from backend
        console.log("Check your details again" + submitted);
        Alert.alert("Error", "Check your details again");
      });
  };
  return (
    <ScrollView>
      <View style={styles.screen}>
        <View style={styles.form}>
          <Text style={styles.signUp}>Sign Up!</Text>
          <Text style={styles.createAccount}>
            Create Account by filling the form below.
          </Text>
        </View>
        <View style={styles.row}>
          <View style={styles.column}>
            <TextInput
              style={[
                styles.textInput,
                submitted && !/^[a-zA-Z]+$/.test(firstName)
                  ? styles.errorInput
                  : null,
              ]}
              placeholder="First name"
              placeholderTextColor="#7a7a52"
              onChangeText={setFirstName}
            />
            {submitted && !/^[a-zA-Z]+$/.test(firstName) && (
              <Text style={styles.errorText}>
                Please enter only {"\n"} alphabetic characters
              </Text>
            )}
          </View>
          <View style={styles.column}>
            <TextInput
              style={[
                styles.textInput,
                submitted && !/^[a-zA-Z]+$/.test(lastName)
                  ? styles.errorInput
                  : null,
              ]}
              placeholder="Last name"
              placeholderTextColor="#7a7a52"
              onChangeText={(text) => setLastName(text)}
            />
            {submitted && !/^[a-zA-Z]+$/.test(lastName) && (
              <Text style={styles.errorText}>
                {" "}
                Please enter only {"\n"} alphabetic characters
              </Text>
            )}
          </View>
        </View>
        <TextInput
          placeholder="Email"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setEmail(text)}
          style={[
            styles.fields,
            submitted && !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
              ? styles.errorInput
              : null,
          ]}
          keyboardType="email-address"
        />
        {submitted && !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email) && (
          <Text style={styles.errorText}> Please enter valid email</Text>
        )}
        <TextInput
          placeholder="Phone Number"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setMobile(text)}
          keyboardType="phone-pad"
          style={[
            styles.fields,
            submitted && (mobile.length != 10 || isNaN(Number(mobile)))
              ? styles.errorInput
              : null,
          ]}
          maxLength={10}
        />
        {submitted && (mobile.length != 10 || isNaN(Number(mobile))) && (
          <Text style={styles.errorText}> Please enter 10 digit number</Text>
        )}
        <TextInput
          placeholder="Select Date of Birth"
          style={[styles.fields, submitted && !dob ? styles.errorInput : null]}
          placeholderTextColor="#7a7a52"
          value={dob ? dob.toISOString().split("T")[0] : ""}
          onFocus={handleDateOfBirthPress}
        />
        {submitted && !dob && (
          <Text style={styles.errorText}>Please select date of birth</Text>
        )}
        {showDatePicker && (
          <DateTimePicker
            value={dob || new Date()}
            mode="date"
            display="default"
            onChange={handleDateOfBirthChange}
          />
        )}
        <View
          style={[
            styles.picker,
            submitted && !gender ? styles.errorInput : null,
          ]}
        >
          <GenderInput
            gender={gender}
            setGender={setGender}
            submitted={submitted}
          />
        </View>
        <TextInput
          style={[
            styles.fields,
            submitted && !street ? styles.errorInput : null,
          ]}
          placeholder="Street"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setStreet(text)}
        />
        {submitted && !street && (
          <Text style={styles.errorText}>Please fill the Street name</Text>
        )}
        <TextInput
          style={[
            styles.fields,
            submitted && !landmark ? styles.errorInput : null,
          ]}
          placeholder="Landmark"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setLandmark(text)}
        />
        {submitted && !landmark && (
          <Text style={styles.errorText}>Please fill the Landmark name</Text>
        )}
        <View
          style={[
            styles.picker,
            submitted && !country ? styles.errorInput : null,
          ]}
        >
          <CountryInput
            country={country}
            setCountry={setCountry}
            submitted={submitted}
          />
        </View>
        <View
          style={[
            styles.picker,
            submitted && !state ? styles.errorInput : null,
          ]}
        >
          <StateInput state={state} setState={setState} submitted={submitted} />
        </View>
        <View
          style={[styles.picker, submitted && !city ? styles.errorInput : null]}
        >
          <CityInput city={city} setCity={setCity} submitted={submitted} />
        </View>
        <TextInput
          style={[
            styles.fields,
            submitted && (pincode.length != 6 || isNaN(Number(pincode)))
              ? styles.errorInput
              : null,
          ]}
          placeholder="Pincode"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setPincode(text)}
          keyboardType="numeric"
          maxLength={6}
        />
        {submitted && (pincode.length != 6 || isNaN(Number(pincode))) && (
          <Text style={styles.errorText}> Please enter 6 digit pincode</Text>
        )}
        <View style={{ flexDirection: "row" }}>
          <View style={{ flexDirection: "column" }}>
            <TextInput
              style={[
                styles.textInput,
                submitted && (!wtCurrent || isNaN(Number(wtCurrent)))
                  ? styles.errorInput
                  : null,
              ]}
              placeholder="Current Weight"
              placeholderTextColor="#7a7a52"
              onChangeText={(text) => setWtCurrent(text)}
              keyboardType="numeric"
            />
            {submitted && (!wtCurrent || isNaN(Number(wtCurrent))) && (
              <Text style={styles.errorText}>
                {" "}
                Please enter {"\n"} Current Weight
              </Text>
            )}
          </View>
          <View style={{ flexDirection: "column" }}>
            <TextInput
              style={[
                styles.textInput,
                submitted && (!wtGoal || isNaN(Number(wtGoal)))
                  ? styles.errorInput
                  : null,
              ]}
              placeholder="Goal Weight"
              placeholderTextColor="#7a7a52"
              onChangeText={(text) => setWtGoal(text)}
              keyboardType="numeric"
            />
            {submitted && (!wtGoal || isNaN(Number(wtGoal))) && (
              <Text style={styles.errorText}>
                {" "}
                Please enter {"\n"} Goal Weight
              </Text>
            )}
          </View>
        </View>
        <TextInput
          style={[
            styles.fields,
            submitted && (!targetSteps || isNaN(Number(targetSteps)))
              ? styles.errorInput
              : null,
          ]}
          placeholder="Target Steps"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setTargetSteps(text)}
          keyboardType="numeric"
        />
        {submitted && (!targetSteps || isNaN(Number(targetSteps))) && (
          <Text style={styles.errorText}> Please enter target steps</Text>
        )}
        <TextInput
          style={[
            styles.fields,
            submitted && !username ? styles.errorInput : null,
          ]}
          placeholder="Username"
          placeholderTextColor="#7a7a52"
          onChangeText={(text) => setUserName(text)}
        />
        {submitted && !username && (
          <Text style={styles.errorText}>Please enter username</Text>
        )}
        <View
          style={[
            styles.inputContainer,
            submitted && !passwordRegex.test(password)
              ? styles.errorInput
              : null,
          ]}
        >
          <TextInput
            placeholder="Password"
            value={password}
            onChangeText={(text) => setPassword(text)}
            secureTextEntry={!showPassword}
            style={[
              styles.input,
              submitted && !passwordRegex.test(password)
                ? styles.errorInput
                : null,
            ]}
          />
          <Ionicons
            name={showPassword ? "eye-off" : "eye"}
            size={24}
            color="#777"
            style={styles.icon}
            onPress={handleTogglePasswordVisibility}
          />
        </View>
        {submitted && !passwordRegex.test(password) && (
          <Text style={styles.errorText}>
            Please enter valid password Requirement : Minimum 8 characters , one
            Uppercase , one lowercase , one digit and one special character
          </Text>
        )}
        <CustomButton
          title={"CREATE ACCOUNT"}
          onPress={handleSubmits}
          style={styles.handleSubmit}
        />
        <View>
          <View style={styles.newAccount}>
            <Text style={styles.newAccountText}>
              Do you already have account?
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate(LOGIN_SCREEN)}>
              <Text style={styles.login}>LOGIN</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
export default RegistrationForm;

const styles = StyleSheet.create({
  screen: {
    backgroundColor: "#FFFCFA",
  },
  form: {
    flexDirection: "column",
    alignContent: "flex-start",
  },
  row: {
    flexDirection: "row",
  },
  column: {
    flexDirection: "column",
  },
  textInput: {
    textAlign: "left",
    marginTop: 25,
    marginLeft: 30,
    paddingLeft: 26,
    paddingTop: 5,
    height: 50,
    width: 161,
    borderWidth: 2,
    borderColor: "grey",
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
    fontSize: 15.4,
  },
  signUp: {
    color: "black",
    fontSize: 25,
    marginTop: 35,
    marginLeft: 10,
    fontWeight: "900",
  },
  createAccount: {
    padding: 10,
    color: "black",
    fontSize: 15,
    fontWeight: "800",
  },
  picker: {
    textAlign: "left",
    marginTop: 15,
    marginLeft: 30,
    marginRight: 30,
    paddingLeft: 10,
    height: 50,
    width: 353,
    borderWidth: 2,
    borderColor: "grey",
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
    marginBottom: 10,
  },
  handleSubmit: {
    marginTop: 40,
    marginBottom: 5,
    paddingBottom: 10,
    paddingTop: 10,
    width: 350,
    alignSelf: "center",
  },
  fields: {
    textAlign: "left",
    marginTop: 15,
    marginLeft: 30,
    marginRight: 30,
    paddingLeft: 26,
    paddingTop: 5,
    height: 50,
    width: 353,
    fontSize: 15.4,
    borderWidth: 2,
    borderColor: "grey",
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
  },
  errorInput: {
    borderColor: "red",
  },
  errorText: {
    color: "red",
    marginLeft: 30,
  },
  pickerError: {
    color: "red",
    marginLeft: -10,
    marginBottom: 20,
  },
  icon: {
    marginTop: 3,
    marginHorizontal: 8,
  },
  inputContainer: {
    flexDirection: "row",
    textAlign: "left",
    marginTop: 15,
    marginLeft: 30,
    marginRight: 30,
    paddingLeft: 26,
    paddingTop: 5,
    height: 50,
    width: 353,
    fontSize: 15.4,
    borderWidth: 2,
    borderColor: "grey",
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
  },
  newAccount: {
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 40,
    paddingTop: 20,
  },
  newAccountText: {
    textAlign: "center",
    fontSize: 16,
  },
  login: {
    paddingLeft: 10,
    textAlign: "center",
    color: "orange",
    fontWeight: "800",
    fontSize: 16,
  },
  input: {
    flex: 1,
  },
});
