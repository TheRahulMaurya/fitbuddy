import React, { useState } from "react";
import { Text, View, TextInput, StyleSheet, Alert, Button } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import CustomButton from "../components/globals/CustomButton";
import ResendOtp from "./ResendOtp";
import axios from "axios";
import { AuthServiceApi } from "../apis/FitBuddyApi";
import { FORGOT_PASSWORD_END_POINT } from "../constants/ApiConstants";
import FitbuddyLogo from "../components/globals/FitbuddyLogo";

interface NavigationProps {
  navigation: any;
  route: any;
}
const ForgotPasswordScreen = ({ navigation, route }: NavigationProps) => {
  const { email } = route.params;

  const [password, setPassword] = useState("");
  const [reenterPassword, setReenterPassword] = useState("");
  const [otp, setOtp] = useState<string>("");
  const [showPassword, setShowPassword] = useState(false);
  const [showReenterPassword, setShowReenterPassword] = useState(false);

  const handlePasswordChange = (text: string) => {
    setPassword(text);
  };

  const handleReenterPasswordChange = (text: string) => {
    setReenterPassword(text);
  };

  const handleOtpChange = (text: string) => {
    setOtp(text);
  };
  const handleTogglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const handleToggleReenterPasswordVisibility = () => {
    setShowReenterPassword(
      (prevShowReenterPassword) => !prevShowReenterPassword
    );
  };

  const validateInput = (input: string): boolean => {
    const regex = /^\d{6}$/;
    return regex.test(input);
  };

  const validateForm = () => {
    return password === reenterPassword;
  };

  const validatePassword = (input: string): boolean => {
    const regex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=!])(?!.*\s).{8,30}$/;
    return regex.test(input);
  };
  const handleSubmit = () => {
    if (!validatePassword(password)) {
      Alert.alert(
        "Invalid password",
        "Please enter valid password Requirement : Minimum 8 characters,one Uppercase , one lowercase , one digit and one special character"
      );
      return;
    }
    if (!validateForm()) {
      Alert.alert("Invalid Form", "Please make sure the passwords match.");
      return;
    }
    if (!validateInput(otp)) {
      Alert.alert("Invalid OTP", "Please enter a six-digit number");
      return;
    } else {
      forgotPassword();
    }
  };
  const headers = {
    "content-type": "application/json",
  };
  //call to verify otp api
  const forgotPassword = () => {
    console.log();
    axios;
    AuthServiceApi.post(
      FORGOT_PASSWORD_END_POINT,
      { password, otp },
      { headers: headers }
    )
      .then((response) => {
        // Handle the success response
        console.log(response.data);
        Alert.alert("OTP verified and password updated", "Please Login");
        navigation.navigate("Login");
      })
      .catch((error) => {
        if (error.response.status == 406) {
          Alert.alert("Inavlid OTP", error.response.data.message);
        } else {
          Alert.alert("Network error, please try again later");
        }
      });
  };

  return (
    <View style={styles.container}>
      <FitbuddyLogo />

      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Password"
          value={password}
          onChangeText={handlePasswordChange}
          secureTextEntry={!showPassword}
          style={styles.input}
        />
        <Ionicons
          name={showPassword ? "eye-off" : "eye"}
          size={24}
          color="#777"
          style={styles.icon}
          onPress={handleTogglePasswordVisibility}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Re enter Password"
          value={reenterPassword}
          onChangeText={handleReenterPasswordChange}
          secureTextEntry={!showReenterPassword}
          style={styles.input}
        />
        <Ionicons
          name={showReenterPassword ? "eye-off" : "eye"}
          size={24}
          color="#777"
          style={styles.icon}
          onPress={handleToggleReenterPasswordVisibility}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          keyboardType="numeric"
          placeholder="OTP"
          value={otp}
          onChangeText={handleOtpChange}
          style={styles.input}
        />
      </View>
      <View style={styles.resendButton}>
        <ResendOtp email={email} />
      </View>
      <View style={styles.button}>
        <CustomButton
          title={"Update Password"}
          style={styles.button}
          onPress={handleSubmit}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
    borderColor: "#ccc",
    borderRadius: 4,
    marginBottom: 16,
    backgroundColor: "#FFFCFA",
    height: "100%",
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: "4%",

    padding: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  input: {
    flex: 1,
    padding: 8,
  },
  icon: {
    marginHorizontal: 8,
  },
  resendButton: {
    flexDirection: "row-reverse",
    marginBottom: "2%",
  },
  button: {
    width: "auto",
    justifyContent: "center",
    marginLeft: "10%",
    marginRight: "10%",
  },
});

export default ForgotPasswordScreen;
