import axios from "axios";
import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { AuthServiceApi } from "../apis/FitBuddyApi";
import { SEND_OTP_END_POINT } from "../constants/ApiConstants";
interface typeProps {
  email: string;
}

const ResendOtp = ({ email }: typeProps) => {
  const [timer, setTimer] = useState(10);
  const [isResendClickable, setIsResendClickable] = useState(false);
  let intervalId: NodeJS.Timeout;

  useEffect(() => {
    if (timer > 0) {
      intervalId = setInterval(() => {
        setTimer((prevTimer) => prevTimer - 1);
      }, 1000);
    } else {
      setIsResendClickable(true);
      clearInterval(intervalId);
    }

    return () => {
      clearInterval(intervalId);
    };
  }, [timer]);

  const headers = {
    "content-type": "application/json",
  };
  const sendOtp = (email: string) => {
    axios;
    AuthServiceApi.post(
      SEND_OTP_END_POINT,
      { email },
      {
        headers: headers,
      }
    )
      .then((response) => {
        // Handle the success response
        console.log(response.data);
      })
      .catch((error) => {
        // Handle the error response
        console.log(error);

        //Alert.alert("Error", "Invalid email");
      });
  };

  const handleResendOTP = () => {
    // Logic to resend OTP
    setIsResendClickable(false);
    setTimer(10);
    // Send OTP axios post call
    sendOtp(email);
  };

  return (
    <View style={styles.container}>
      {!isResendClickable && <Text>Resend OTP in: {timer} seconds</Text>}
      {isResendClickable && (
        <TouchableOpacity onPress={handleResendOTP}>
          <Text style={styles.resendButton}>Resend OTP</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    marginRight: "10%",
  },
  resendButton: {
    marginTop: 10,
    color: "#EB6D00",
  },
});

export default ResendOtp;
