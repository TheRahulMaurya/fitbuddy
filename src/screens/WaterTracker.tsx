import {
  Alert,
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  Pressable
} from "react-native";
import React,{useState} from "react";
import { Card } from "@rneui/themed";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
import moment from "moment";

import Header from "../components/globals/Header";
import { getToken, getUserIdFromToken } from "../utils/JwtUtil";

//Water Tracker Screen
interface NavigationProps {
  navigation: any;
}

const WaterTracker = ({ navigation }: NavigationProps) => {

  const [count, setCount] = useState(0); // shown below + and - sign
  const [glassesForADay,setGlassesForADay] = useState(0); // shown at out of 12 glasses
  const [isFlipped, setIsFlipped] = useState(false); //to get the save tex,image of boy driniking water and disable + until the 'Save' text is clicked
  const [waterId,setWaterId] = useState(0); // to store the water id created for user for a day
  const [modalVisible, setModalVisible] = useState(false);
 
  function flipGlassImage() {
    setIsFlipped(!isFlipped);
  }

  function increment() {
    setCount(function (prevCount) {
      return (prevCount += 1);
    });
  }

  function decrement() {
    setGlassesForADay(function (prevGlassCount) {
      if (prevGlassCount > 0) {
        return (prevGlassCount -= 1);
      } else {
        return (prevGlassCount = 0);
      }
    });
  }

  const getUserId = async()=>{
    
    return userid;
  }

  // contains get and post request
  const getWaterIntakeForADay = async()=>{
    let currentDate = moment().format("DD-MM-YYYY");
    let token = await getToken();
    console.log("token in water intake screen---- : " + token);
    let userid = getUserIdFromToken(token);
    console.log("User id is : " + userid);
    const config = {
      headers: {
        Date: currentDate,
      },
    };

    const data = {
      "noOfGlasses" :count
    };

    axios.get('http://192.168.1.6:9091/api/v1/users/'+userid+'/water',config)
    .then((response)=>{
      console.log('came inside get request');
      console.log('get call response is ... ',response.data);
      setGlassesForADay(response.data.data.noOfGlasses);
      setWaterId(response.data.data.userWaterId);
    })
    .catch((error)=>{
      console.log(error.response.status);
      if(error.response.status==404){
        console.log('came inside post request');
        axios.post('http://192.168.1.6:9091/api/v1/users/'+userid+'/water',data)
        .then((response)=>{
          console.log('post call response is ... ',response.data);
          setWaterId(response.data.data.userWaterId);
        })
        .catch((error)=>{
          console.log(error);
        })
      }
      else{
        console.log(error.response.status);
      }
    });
   } 

  React.useEffect(()=>{
  getWaterIntakeForADay();
  },[]);

  const putWaterIntakeForADay = async(count:any)=>{
    console.log('came inside put request');
    getWaterIntakeForADay();
    const alreadyPresentWater=glassesForADay;
    console.log('water already present ',alreadyPresentWater);
    const totalWater=alreadyPresentWater+count;
    const data = {
      "userWaterId": waterId,
      "noOfGlasses" : totalWater
    };
    console.log('going to add this much water',totalWater);
    let token = await getToken();
    console.log("token in water intake screen---- : " + token);
    let userid = getUserIdFromToken(token);
    console.log("User id is : " + userid);
    axios.put('http://192.168.1.6:9091/api/v1/users/'+userid+'/water',data)
    .then((response)=>{
      console.log('put call response is ... ',response.data);
      console.log('added water ... ',response.data.data.noOfGlasses);
      setGlassesForADay(response.data.data.noOfGlasses);
    })
    .catch((error)=>{
      console.log(error.response);
    })
  }

  return (
    <View style={styles.screen}>
      <View style={styles.headerAlign}>
        <Header navigation={navigation} />
      </View>
      <Card containerStyle={styles.card}>
        {
          glassesForADay<12 ? 
          <View style={styles.textAlign}>
          <Text style={styles.glassText}>{glassesForADay} of 12 Glasses({glassesForADay*250}ml)</Text>

          <Modal
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.popUp}>
              <Text style={styles.popUpText}>This is Under Development</Text>
              <Pressable
                onPress={() => setModalVisible(!modalVisible)}
                style={styles.modal}
              >
                <Text style={styles.closeText}>Close</Text>
              </Pressable>
            </View>
          </Modal>
          <Pressable onPress={() => setModalVisible(true)}>
            <MaterialCommunityIcons
              name="pencil"
              color={"blue"}
              size={20}
              style={styles.saveWater}
            />
          </Pressable>
        </View> :
        <View>
          <Text style={styles.text}>Congratulations you have achieved your goal for today !!</Text>
        </View>
        }
      </Card>
      <View>
        <Card containerStyle={styles.waterConsumed}>
          <View style={styles.counterAlign}>
            <TouchableOpacity style={styles.counterButton} onPress={decrement}>
              <Text style={styles.counterText}>-</Text>
            </TouchableOpacity>
            <Image
              source={{
                uri: isFlipped
                  ? "https://gitlab.com/TheRahulMaurya/fitbuddy/uploads/0f0b4328e67c05d422438fedad192ae7/saveWater.PNG" 
                  : "https://gitlab.com/kritisonker95/fitbuddy/uploads/e836be56114ec3ca0bb0dcef4008eeae/water.png"
              }}
              style={styles.glassImage}
            />
            {glassesForADay < 12 ? (
              <TouchableOpacity
                style={
                  isFlipped?styles.counterButtonDisabled:styles.counterButton}
                disabled={isFlipped}
                onPress={() => {
                  console.log('pressed me +')
                  if (!isFlipped) {
                    increment();
                    flipGlassImage();
                  }
                }}
              >
                {console.log('count is ',count)}
                <Text style={styles.counterText}>+</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.counterButtonDisabled}
                disabled={true}
              >
                <Text style={styles.counterText}>+</Text>
              </TouchableOpacity>)}
            {isFlipped && (
              <TouchableOpacity onPress={()=>{
                putWaterIntakeForADay(count);
                flipGlassImage();
                setCount(0);
              }
              }>
                <Text style={styles.flipSave}>Save</Text>
              </TouchableOpacity>
            )}
          </View>
          <Text style={styles.waterIntake}>
            {count} Glass ({count * 250}ml)
          </Text>
        </Card>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate("ReminderScreen")}>
          <Card containerStyle={styles.card}>
            <Image
              source={{
                uri: "https://gitlab.com/kritisonker95/fitbuddy/uploads/dd5be6a79d18d3fbc99e0e16ad153e50/Water.jpg",
              }}
              style={styles.image}
            />

            <Text style={styles.reminderText}>Set Reminder</Text>
          </Card>
        </TouchableOpacity>
      </View>
      <Card containerStyle={styles.card}>
        <Text style={styles.quoteText}>
          Blood is more than 90 percent water, and blood carries oxygen to
          different parts of the body.
        </Text>
      </Card>
    </View>
  );
};
const styles = StyleSheet.create({
  screen: {
    backfaceVisibility: "hidden",
    backgroundColor: "white",
    height: 1024,
  },
  headerAlign: {
    marginTop: 50,
    marginLeft: -30,
  },
  card: {
    backgroundColor: "white",
    width: 400,
    height: 75,
    paddingTop: 0,
    borderRadius: 10,
    textAlign: "center",
    shadowColor: "#171717",
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 4,
    marginTop: 60,
    marginBottom: 10,
    elevation: 4,
    flexDirection: "column",
    alignSelf: "center",
  },
  textAlign: {
    flexDirection: "row",
  },
  glassText: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    paddingTop: 24,
    paddingLeft: 10,
  },
  text: {
    fontWeight: "bold",
    fontSize: 14,
    textAlign: "center",
    paddingTop: 24,
    paddingLeft: 10,
    color: 'green',
  },
  popUp: {
    alignSelf: "center",
    backgroundColor: "#f2f2f2",
    borderRadius: 20,
    padding: 35,
    height: 260,
    width: 260,
    marginTop: 300,
    alignItems: "center",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 20,
  },
  popUpText: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: "400",
  },
  modal: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: "#191966",
    marginTop: 40,
  },
  closeText: {
    fontSize: 20,
    color: "white",
  },
  cards: {
    height: 200,
    width: 300,
  },
  saveWater: {
    paddingTop: 26,
    paddingLeft: 120,
    marginRight: 50,
  },
  flipSave: {
    marginLeft: -150,
    width:60,
    marginTop: 25,
    fontWeight: "900",
    fontSize: 25,
    color: "black",
  },
  counterAlign: {
    flexDirection: "row",
  },
  counterButton: {
    marginLeft: 40,
    width: 50,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 40,
    borderRadius: 35,
    backgroundColor: "orange",
    marginTop: 20,
  },
  counterButtonDisabled: {
    marginLeft: 40,
    width: 50,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 40,
    borderRadius: 35,
    backgroundColor: "#bdb1aa",
    marginTop: 20,
  },
  counterText: {
    textAlign: "center",
    position: "absolute",
    fontSize: 30,
    color: "white",
  },
  waterIntake: {
    fontSize: 24,
    paddingLeft: 50,
    paddingTop: 30,
    alignSelf: "center",
  },
  image: {
    height: 60,
    width: 40,
    borderRadius: 50,
    marginLeft: 50,
  },
  waterConsumed: {
    height: 200,
    width: 400,
    flexDirection: "row",
    backgroundColor: "white",
    alignSelf: "center",
    marginTop: 60,
    borderRadius: 10,
    shadowColor: "#171717",
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  glassImage: {
    height: 75,
    width: 75,
    borderRadius: 50,
    marginLeft: 50,
  },
  reminderText: {
    fontWeight: "400",

    fontSize: 20,
    textAlign: "center",
    marginTop: -40,
  },
  quoteText: {
    fontWeight: "400",
    alignContent: "space-around",
    fontSize: 16,
    textAlign: "center",
    paddingTop: 20,
  },
});

export default WaterTracker;
