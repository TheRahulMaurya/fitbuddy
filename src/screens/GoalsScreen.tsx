import * as React from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Card } from "@rneui/themed";
import CaloriesIntake from "./CaloriesIntake";
import CustomButton from "../components/globals/CustomButton";
import { FOOD_TYPE_SCREEN, HOME_SCREEN } from "../constants/ScreenNames";
import CalorieCountChart from "../components/ResponsiveLineChart";
interface NavigationProps {
  navigation: any;
}
//Goals Screen
const GoalsScreen = ({ navigation }: NavigationProps) => {
  return (
    <View style={styles.card}>
      <Image
        source={{
          uri: "https://i.gifer.com/8i9q.gif",
        }}
        style={styles.image}
      />
      <Text style={styles.text}>Congratulations</Text>
      <Text style={styles.textTrack}>
        You have successfully tracked your meal
      </Text>
      <View style={styles.alignButtons}>
        <Card containerStyle={styles.cardStyle}>
          {/* <CalorieScreen /> */}
          <CaloriesIntake />
          <TouchableOpacity
            onPress={() => navigation.navigate(FOOD_TYPE_SCREEN)}
            style={styles.addItems}
          >
            <Text style={styles.addItemsText}>+ ADD MORE ITEMS</Text>
          </TouchableOpacity>
        </Card>
      </View>
      <CustomButton
        onPress={() => navigation.navigate(HOME_SCREEN)}
        style={styles.homeButton}
        title="HOME"
      ></CustomButton>
      <CustomButton
        onPress={() => navigation.navigate(HOME_SCREEN)}
        style={styles.checkProgressButton}
        title="CHECK PROGRESS"
        setWhite={true}
      ></CustomButton>
    </View>
  );
};
const styles = StyleSheet.create({
  card: {
    marginTop: 70,
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  text: {
    fontSize: 24,
    fontWeight: "bold",
  },
  textTrack: {
    fontSize: 12,
    fontWeight: "400",
  },
  image: {
    width: 80,
    height: 80,
  },
  calorieAlign: {
    flex: 1,
    alignItems: "center",
  },
  addItems: {
    marginTop: 20,
    // backgroundColor: "white",
    backgroundColor: "yellow",
    marginLeft: "40%",
    height: 60,
  },
  addItemsText: {
    fontSize: 16,
    color: "orange",
    textAlign: "right",
    marginRight: 60,
    // marginLeft: "50%",
    marginTop: 30,
    fontWeight: "900",
    alignItems: "center",
    justifyContent: "center",
  },
  homeButton: {
    marginBottom: 10,
  },
  checkProgressButton: {
    marginBottom: 50,
  },
  alignButtons: {
    flex: 1,
    alignItems: "center",
  },
  cardStyle: {
    backgroundColor: "pink",
    width: 450,
    height: 20,
    alignItems: "center",
    paddingTop: 0,
    elevation: 20,
  },
});

export default GoalsScreen;
