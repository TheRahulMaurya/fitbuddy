import React, { useState, useEffect } from "react";
import { ActivityIndicator, View, Image, Text } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

interface NavigationProps {
  navigation: any;
}

const SplashScreen = ({ navigation }: NavigationProps) => {
  //State for ActivityIndicator animation
  const [animating, setAnimating] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setAnimating(false);
      //Check if userCred is set or not
      //If not then send for Login
      //else send to Profile Screen
      AsyncStorage.getItem("userCred").then((value) =>
        navigation.replace(value === null ? "Login" : "Footer")
      );
    }, 3000);
  }, []);
  return (
    <View className="flex flex-1 justify-center items-center flex-col bg-white">
      <Image
        source={{
          uri: "https://gitlab.com/makarandlad2/fitbuddy/uploads/af6008f2b46f0d3a48794419b5ae2d22/aboutHealth.PNG",
        }}
        style={{ width: "50%", resizeMode: "contain", margin: 30 }}
      />

      <Text className="font-extrabold text-xl text-orange-600">
        Welcome to FitBuddy
      </Text>

      <ActivityIndicator
        animating={animating}
        color="#39ac39"
        size="large"
        className="flex items-center h-20"
      />
    </View>
  );
};
export default SplashScreen;
