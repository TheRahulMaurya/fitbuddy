import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Header from "../components/globals/Header";
import { Card } from "react-native-elements";
//Weight Tracker Screen
interface NavigationProps {
  navigation: any;
}
const WeightTracker = ({ navigation }: NavigationProps) => {
  return (
    <View style={styles.screen}>
      <View style={styles.hearderAlign}>
        <Header navigation={navigation} />
        <View>
          <Card containerStyle={styles.comingSoon}>
            <Text style={styles.comingSoonText}>Coming Soon</Text>
          </Card>
          <Text style={styles.text}>WEIGHT TRACKER</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    backgroundColor: "white",
    height: "100%",
  },
  comingSoon: {
    height: 80,
    width: 368,
    marginLeft: 20,
    marginRight: 20,
    alignItems: "center",
    backgroundColor: "black",
  },
  comingSoonText: {
    color: "red",
    fontSize: 24,
    fontWeight: "bold",
  },
  hearderAlign: {
    marginTop: 50,
  },
  text: {
    fontSize: 20,
    fontWeight: "700",
    marginHorizontal: 50,
    marginTop: 50,
    alignContent: "center",
    textAlign: "center",
  },
});

export default WeightTracker;
