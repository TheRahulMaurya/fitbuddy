import React, { useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  Pressable,
  Alert,
  Image,
} from "react-native";
import Header from "../components/globals/Header";
import { Card } from "@rneui/themed";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useState } from "react";
import CircularProgress from "react-native-circular-progress-indicator";
import { getToken, getUserIdFromToken } from "../utils/JwtUtil";
import { GET_UNIQUE_USER_END_POINT } from "../constants/ApiConstants";
import { FitBuddyApi } from "../apis/FitBuddyApi";
import { useIsFocused } from "@react-navigation/native";
//Steps Tracker Screen
interface NavigationProps {
  navigation: any;
}
const StepsTracker = ({ navigation }: NavigationProps) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [targetSteps, setTargetSteps] = useState(0);
  const isFocused = useIsFocused();
  const getData = async () => {
    let token = await getToken();
    console.log("token---- : " + token);
    let userId = getUserIdFromToken(token);
    console.log("User id is : " + userId);
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    FitBuddyApi.get(GET_UNIQUE_USER_END_POINT + userId, config).then(
      (response) => {
        const targetStepsValue = response.data.data.targetSteps;
        setTargetSteps(targetStepsValue);
        console.log(targetStepsValue);
      }
    );
  };
  useEffect(() => {
    console.log("----------inside useEffect ----------------");
    // Call only when screen open or when back on screen
    if (isFocused) {
      console.log("----------Get the target Steps ----------------");
      getData();
    }
  }, [navigation, isFocused]);
  return (
    <View style={styles.screen}>
      <View style={styles.hearderAlign}>
        <Header navigation={navigation} />
        <View>
          <Text style={styles.text}>Track your Steps</Text>
        </View>
        <Card containerStyle={styles.mainCard}>
          <View style={{ flexDirection: "row" }}>
            <Image
              source={{
                uri: "https://gitlab.com/kritisonker95/fitbuddy/uploads/2807e97a27c2ced57261e077c33fb00d/Steps.png",
              }}
              style={styles.image}
            />
            <Text style={styles.imageText}>
              Set Target Steps {"\n"} {targetSteps} Steps
            </Text>

            <Modal
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.popUp}>
                <Text style={styles.popUpText}>This is Under Development</Text>
                <Pressable
                  onPress={() => setModalVisible(!modalVisible)}
                  style={styles.modal}
                >
                  <Text style={styles.closeText}>Close</Text>
                </Pressable>
              </View>
            </Modal>
            <Pressable onPress={() => setModalVisible(true)}>
              <MaterialCommunityIcons
                name="pencil"
                color={"black"}
                size={24}
                style={styles.icon}
              />
            </Pressable>
          </View>
        </Card>
        <View style={styles.progressBarCard}>
          <CircularProgress
            value={8000}
            radius={120}
            progressValueColor={"green"}
            maxValue={targetSteps}
            titleStyle={{ fontWeight: "bold" }}
            activeStrokeColor={"#062985"}
            valueSuffix=" Steps"
            valueSuffixStyle={{}}
            progressValueFontSize={30}
            inActiveStrokeWidth={20}
            activeStrokeWidth={20}
          />
        </View>
        <View style={styles.cardsView}>
          <Card containerStyle={styles.subCards}>
            <Text style={styles.cardText}>Distance {"\n"} 5.5km</Text>
          </Card>
          <Card containerStyle={styles.subCards}>
            <Text style={styles.cardText}>Steps {"\n"} 8000</Text>
          </Card>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    backgroundColor: "white",
    height: "100%",
  },
  mainCard: {
    elevation: 4,
    marginTop: 50,
  },
  hearderAlign: {
    marginTop: 50,
  },
  image: {
    height: 60,
    width: 50,
    marginLeft: 20,
  },
  imageText: {
    marginLeft: 10,
    marginTop: 7,
    fontWeight: "bold",
    fontSize: 18,
  },
  icon: {
    marginTop: 17,
    marginRight: 20,
    marginLeft: 100,
  },
  progressBarCard: { marginTop: 40, alignSelf: "center" },
  text: {
    fontSize: 24,
    fontWeight: "bold",
    marginHorizontal: 50,
    marginTop: 20,
    alignContent: "center",
    textAlign: "center",
  },
  popUp: {
    alignSelf: "center",

    backgroundColor: "#f2f2f2",
    borderRadius: 20,
    padding: 35,
    height: 260,
    width: 260,
    marginTop: 300,
    alignItems: "center",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 20,
  },
  popUpText: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: "400",
  },
  modal: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: "#191966",
    marginTop: 40,
  },
  closeText: {
    fontSize: 20,
    color: "white",
  },
  cardsView: {
    flexDirection: "row",
    alignSelf: "center",
    height: 300,
    width: 300,
    marginTop: 50,
  },
  cardText: {
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 28,
    fontSize: 20,
  },
  subCards: { height: 120, width: 120, elevation: 7 },
});

export default StepsTracker;
