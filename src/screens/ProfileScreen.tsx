import { AntDesign } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import CustomAvatar from "../components/profile/CustomAvatar";
import Detail from "../components/profile/Detail";
import { getToken, getUserFromToken } from "../utils/JwtUtil";
import { getUserAge } from "../utils/UserUtil";
const ProfileScreen: React.FC = ({ navigation }: any) => {
  const name = "Rahul Maurya";
  const email = "rahulmaurya@gmail.com";
  const [user, setUser] = useState(null);
  const getUser = async () => {
    const token = await getToken();
    const user = getUserFromToken(token);
    setUser(user);
  };
  const onPress = (e: any) => {
    e.preventDefault();
    console.log("Button clicked!!  ");
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <View style={styles.profile}>
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <CustomAvatar
            title={user ? user.firstName[0] : "X"}
            name={user ? user.firstName + user.lastName : "Unknown"}
            email={user && user.email}
          />
          <View style={styles.detailsContainer}>
            <Detail
              detail={user ? getUserAge(user.dob) : "26"}
              detailTitle={"Age"}
            />
            <Detail detail={"2000"} detailTitle={"Steps"} />
            <Detail detail={"4"} detailTitle={"Glasses"} />
            <Detail
              detail={user ? user.userWeight.wtCurrent : "70"}
              detailTitle={"Weight"}
            />
          </View>
        </View>
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.listText}>Settings</Text>
        <AntDesign
          style={styles.arrow}
          name="right"
          size={24}
          color="black"
          onPress={onPress}
        />
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.listText}>Update app</Text>
        <AntDesign
          style={styles.arrow}
          name="right"
          size={24}
          color="black"
          onPress={onPress}
        />
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.listText}>About</Text>
        <AntDesign
          style={styles.arrow}
          name="right"
          size={24}
          color="black"
          onPress={onPress}
        />
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.listText}>Logout</Text>
        <AntDesign
          style={styles.arrow}
          name="right"
          size={24}
          color="black"
          onPress={onPress}
        />
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.listText}></Text>
      </View>
    </View>
  );
};
export default ProfileScreen;

const styles = StyleSheet.create({
  profile: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
  },
  container: {
    backgroundColor: "#FFFFFF",
    width: "100%",
    height: "50%",
    transform: [{ scaleX: 2 }],
    borderBottomStartRadius: 200,
    borderBottomEndRadius: 200,
    overflow: "hidden",
    marginBottom: 10,
  },
  topContainer: {
    backgroundColor: "#5FC5FF",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    transform: [{ scaleX: 0.5 }],
  },
  detailsContainer: {
    marginTop: 40,
    width: "90%",
    borderRadius: 10,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

  parent: {
    height: "40%",
    width: "100%",
    transform: [{ scaleX: 2 }],
    borderBottomStartRadius: 200,
    borderBottomEndRadius: 200,
    overflow: "hidden",
  },
  child: {
    flex: 1,
    transform: [{ scaleX: 0.5 }],
    backgroundColor: "yellow",
    alignItems: "center",
    justifyContent: "center",
  },
  listContainer: {
    display: "flex",
    flexDirection: "row",
    // marginTop: 10,
    width: "100%",
    height: 60,
    // backgroundColor: "grey",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 10,
    borderTopWidth: 2,
    borderColor: "#C5C6D0",
  },
  listText: {
    fontSize: 18,
    fontFamily: "sans-serif-medium",
  },
  arrow: {
    marginRight: 10,
  },
});
