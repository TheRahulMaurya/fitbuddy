import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { Picker } from "@react-native-picker/picker";
import { getToken, getUserIdFromToken } from "../utils/JwtUtil";
import { FitBuddyApi } from "../apis/FitBuddyApi";
import ResponsiveLineChart from "../components/ResponsiveLineChart";

const ChartScreen = () => {
  const [selectedMonth, setSelectedMonth] = useState("01");
  const [chartData, setChartData] = useState([{ x: 1, y: 0 }]);

  //use effect with selected month as the change parameter
  useEffect(() => {
    console.log("chartscreen rendered");
    getCalorieData();
  }, [selectedMonth]);

  //first time on rendering for jan and then call each time the selected month change
  //with the data format it according to the x y format and set it in the chartdata
  const getCalorieData = async () => {
    let token = await getToken();
    let userId = getUserIdFromToken(token);
    console.log("User id is : " + userId);

    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    let url = "/intakes/users/" + userId + "/month/" + selectedMonth + "-2023";

    FitBuddyApi.get(url, config)
      .then((response) => {
        console.log(response.data.data);
        const convertedArray = response.data.data.map((item) => {
          return { x: item.day, y: item.calories };
        });
        console.log(convertedArray);
        if (convertedArray.length > 0) {
          console.log("array is not empty");
          setChartData(convertedArray);
        } else {
          console.log("array is empty");
          setChartData([{ x: 1, y: 0 }]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleMonthChange = (selectedMonth) => {
    setSelectedMonth(selectedMonth);
  };

  return (
    <View style={{ backgroundColor: "#ffff" }}>
      <View style={{ alignItems: "center" }}>
        <Picker
          style={{ width: 150 }}
          selectedValue={selectedMonth}
          onValueChange={handleMonthChange}
        >
          <Picker.Item label="January" value="01" />
          <Picker.Item label="February" value="02" />
          <Picker.Item label="March" value="03" />
          <Picker.Item label="April" value="04" />
          <Picker.Item label="May" value="05" />
          <Picker.Item label="June" value="06" />
          <Picker.Item label="July" value="07" />
          <Picker.Item label="August" value="08" />
          <Picker.Item label="September" value="09" />
          <Picker.Item label="October" value="10" />
          <Picker.Item label="November" value="11" />
          <Picker.Item label="December" value="12" />
        </Picker>
      </View>

      <ResponsiveLineChart
        chartData={chartData}
        xAxisLabel={"Days"}
        yAxisLabel={"Calorie"}
        xMinValue={1}
        xMaxValue={31}
        yMinValue={0}
        yMaxValue={4500}
      />
    </View>
  );
};

export default ChartScreen;
