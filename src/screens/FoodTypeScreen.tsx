import { AntDesign } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useTailwind } from "tailwind-rn/dist";
import Item from "../components/Item";
import CustomButton from "../components/globals/CustomButton";
import { FOOD_SELECTION_SCREEN, HOME_SCREEN } from "../constants/ScreenNames";
import { mealTypeMapper } from "../mapper";
import {
  clearCart,
  fetchFoodType,
  selectFoodType,
  selectFoodTypes,
} from "../store";
import { fetchFoodIntakeForDay } from "../store/thunks/FoodIntakeThunk";
import { selectFoodIntakeForDay } from "../store/slices/FoodIntakeForDaySlice";
import { useIsFocused } from "@react-navigation/native";

const FoodTypeScreen = ({ navigation }: any) => {
  const dispatch = useDispatch();
  const { data: types, isLoading, error } = useSelector(selectFoodTypes);
  const { data: intakes } = useSelector(selectFoodIntakeForDay);
  const [disabled, setDisabled] = useState(true);
  const isFocused = useIsFocused();

  interface typeProps {
    id: number;
    imageUrl: string;
    title: string;
  }

  useEffect(() => {
    setDisabled(true);
    types &&
      types.forEach((type) => {
        if (type.isChecked === true) {
          setDisabled(false);
        }
      });
  }, [types]);

  useEffect(() => {
    // Call only when screen open or when back on screen
    if (isFocused) {
      dispatch(clearCart());
      dispatch(fetchFoodType());
      dispatch(fetchFoodIntakeForDay(1));
    }
  }, [navigation, isFocused]);

  const onClose = () => {
    console.log("close pressed!!");
    navigation.navigate(HOME_SCREEN);
  };

  function getCaloriesFromFoodType(foodType: string) {
    var calorieTotal = 0;
    intakes &&
      intakes.data.forEach((intake: any) => {
        if (intake.mealType.mealType === foodType) {
          intake.userFoodIntakeItems.map((userFoodIntakeItem: any) => {
            calorieTotal =
              calorieTotal +
              userFoodIntakeItem.quantity *
                userFoodIntakeItem.foodPropMaps.foodMacros.calories;
          });
        }
      });
    return calorieTotal;
  }

  const onPressHandler = () => {
    console.log("Button clicked");
    console.log(types);
    const selectedFoodType = types.filter(
      (type: any) => type.isChecked === true
    );
    console.log(selectedFoodType);
    dispatch(selectFoodType(selectedFoodType[0]));
    navigation.navigate(FOOD_SELECTION_SCREEN);
  };
  const tw = useTailwind();

  return (
    <View
      style={{
        ...tw("flex-auto flex-col-reverse h-full w-full"),
        backgroundColor: "#ACACAC",
        // ...appStyles.screenView,
      }}
    >
      <View style={tw("flex-col-reverse h-2/3 ")}>
        <View style={tw("w-full h-4/5 bg-white rounded-t-3xl")}>
          <Text style={tw("py-3 text-2xl font-semibold ml-5 mt-6")}>
            Select meal type
          </Text>
          <ScrollView>
            {isLoading && <Text>Loading....</Text>}
            {error && <Text>{error.message}</Text>}
            {console.log("print types")}
            {console.log(types)}
            {console.log("isLoading " + isLoading)}
            {/* {console.log(types)} */}
            {types ? (
              types.map((type: any) => (
                <Item
                  key={type.mealTypeId}
                  foodId={type.mealTypeId}
                  image={type.imageUrl}
                  name={mealTypeMapper(type.mealType)}
                  detailText={`${getCaloriesFromFoodType(
                    type.mealType
                  )} of 625 Calories Consumed`}
                  rightIconType={"radio"}
                  isChecked={type.isChecked}
                />
              ))
            ) : (
              <Text></Text>
            )}
          </ScrollView>

          <View style={styles.button}>
            <CustomButton
              title={"CONTINUE"}
              onPress={onPressHandler}
              disabled={disabled}
            />
          </View>
        </View>
        <View style={tw("flex-row-reverse pb-4 ml-3")}>
          <AntDesign name="close" size={30} color="black" onPress={onClose} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    margin: "4%",
  },
});

export default FoodTypeScreen;
