import { AntDesign } from "@expo/vector-icons";
import React, { useState } from "react";
import { Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useTailwind } from "tailwind-rn/dist";
import Item from "../components/Item";
import CustomButton from "../components/globals/CustomButton";
import HorizontalScroll from "../components/globals/HorizontalScroll";
import QuantityToggle from "../components/globals/QuantityToggle";
import { FOOD_SELECTION_SCREEN } from "../constants/ScreenNames";
import { mealTypeMapper } from "../mapper";
import { addFood, selectedFood, selectedFoodType } from "../store";
import { getFoodDetail } from "../utils/FoodUtil";

const SelectItemScreen = ({ navigation }) => {
  const [quantity, setQuantity] = useState(1);
  const [selectedPropIndex, setSelectedPropIndex] = useState(0);
  const dispatch = useDispatch();
  const tw = useTailwind();
  const food = useSelector(selectedFood);
  const type = useSelector(selectedFoodType);

  const onIncreament = () => {
    setQuantity(quantity + 1);
  };
  const onDecreament = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const onClose = () => {
    console.log("close pressed!!");
    navigation.navigate(FOOD_SELECTION_SCREEN);
  };

  const onAddFood = () => {
    console.log("inside onAddFood");
    const foodToAdd = {
      quantity: quantity,
      foodId: food.foodId,
      foodPropMapId: food.foodPropMaps[selectedPropIndex].foodPropMapId,
    };
    dispatch(addFood(foodToAdd));
    navigation.navigate(FOOD_SELECTION_SCREEN);
  };

  const getPropData = (foodPropMaps: any) => {
    return foodPropMaps.map(
      (foodPropMap: any) => foodPropMap.foodProportion.proportion
    );
  };

  return (
    <View
      style={{
        ...tw("bg-orange-200 flex-auto flex-col-reverse h-full w-full mb-0"),
        backgroundColor: "#ACACAC",
      }}
    >
      <View style={tw("flex-col-reverse h-2/3")}>
        <View style={tw("w-full h-4/5 bg-white rounded-t-3xl")}>
          <Text style={tw("py-3 text-2xl font-semibold ml-3 mt-6")}>
            Select item for {mealTypeMapper(type.mealType).toLowerCase()}
          </Text>
          <Item
            image={food.foodImageUrl}
            name={food.foodName}
            detailText={getFoodDetail(food, quantity, selectedPropIndex)}
          />
          <View style={tw("items-center h-1/2 justify-evenly mt-14")}>
            <QuantityToggle
              count={quantity}
              iconSize={24}
              width={196}
              height={58}
              increament={onIncreament}
              decreament={onDecreament}
            />
            <HorizontalScroll
              propData={getPropData(food.foodPropMaps)}
              selectedIndex={selectedPropIndex}
              setSelected={setSelectedPropIndex}
            />
            <CustomButton title={"ADD"} onPress={onAddFood} />
          </View>
        </View>
        <View style={tw("flex-row-reverse pb-4 ml-3")}>
          <AntDesign name="close" size={30} color="black" onPress={onClose} />
        </View>
      </View>
    </View>
  );
};

export default SelectItemScreen;
