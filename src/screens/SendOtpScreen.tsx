import React, { useState } from "react";
import { Alert, Text, TextInput, View, StyleSheet } from "react-native";
import { Button } from "react-native-elements";
import CustomButton from "../components/globals/CustomButton";
import axios from "axios";
import { AuthServiceApi } from "../apis/FitBuddyApi";
import { SEND_OTP_END_POINT } from "../constants/ApiConstants";
import FitbuddyLogo from "../components/globals/FitbuddyLogo";
interface NavigationProps {
  navigation: any;
  route: any;
}
interface FormErrors {
  emailError: string;
}
const SendOtpScreen = ({ navigation, route }: NavigationProps) => {
  const { nextScreen } = route.params;

  const [email, setEmail] = useState("");

  const [errors, setErrors] = useState<FormErrors>({
    emailError: "",
  });

  const handleEmailchange = (text: string) => {
    setEmail(text);
  };

  //handle validation
  const handleValidation = () => {
    let formIsValid = true;
    const updatedErrors: FormErrors = {
      emailError: "",
    };

    if (!email) {
      updatedErrors.emailError = "Email is required";
      formIsValid = false;
    } else if (!/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/.test(email)) {
      updatedErrors.emailError = "Invalid email format";
      formIsValid = false;
    }

    setErrors(updatedErrors);
    return formIsValid;
  };

  const handleSubmit = () => {
    if (handleValidation()) {
      // Perform sendotp logic
      sendOtp(email);
    }
  };

  const headers = {
    "content-type": "application/json",
  };
  const sendOtp = (email: string) => {
    axios;
    AuthServiceApi.post(
      SEND_OTP_END_POINT,
      { email },
      {
        headers: headers,
      }
    )
      .then((response) => {
        // Handle the success response
        console.log(response.data);
        Alert.alert("OTP sent", "Please check your email for otp");
        navigation.navigate(nextScreen, { email: email });
      })
      .catch((error) => {
        // Handle the error response
        if (error.response.status == 404) {
          console.log(error);
          Alert.alert("Error", error.response.data.message + email);
        } else {
          Alert.alert("Network error, please try again later");
        }
      });
  };

  return (
    <View style={styles.container}>
      <FitbuddyLogo />
      <View style={styles.text}>
        <Text>Please Enter your email address.</Text>
      </View>
      <TextInput
        placeholder="Email"
        value={email}
        onChangeText={handleEmailchange}
        style={styles.input}
      ></TextInput>
      {errors.emailError ? (
        <Text style={styles.error}>{errors.emailError}</Text>
      ) : null}
      <View style={styles.button}>
        <CustomButton
          title={"CONTINUE"}
          style={styles.button}
          onPress={handleSubmit}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    marginLeft: "10%",
    marginRight: "10%",

    padding: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  button: {
    width: "auto",
    justifyContent: "center",
    marginLeft: "10%",
    marginRight: "10%",
    marginTop: "10%",
  },
  container: {
    height: "100%",
    backgroundColor: "#FFFCFA",
  },
  text: {
    margin: 10,
    marginLeft: "10%",
  },
  error: {
    color: "red",
    marginBottom: 10,
    marginLeft: 50,
  },
});

export default SendOtpScreen;
