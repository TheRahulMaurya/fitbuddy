import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import WeightGoalInfo from "./WeightGoalInfo";
interface NavigationProps {
  navigation: any;
  route: any;
}
const WeightTrackerScreen = ({ navigation, route }: NavigationProps) => {
  //put this in useeffect and get the weightgoal details
  const weightGoalData = {
    currentWeight: 100.5,
    targetWeight: 75.6,
    timeRemaining: 15,
  };

  const [currWeight, setCurrWeight] = useState<Number>(
    weightGoalData.currentWeight
  );
  const [trWeight, setTrWeight] = useState<Number>(weightGoalData.targetWeight);

  const handleCurrentWeightChange = (weight: number) => {
    setCurrWeight(weight);
  };

  const handleTargetWeightChange = (weight: number) => {
    setTrWeight(weight);
  };

  return (
    <View style={styles.container}>
      <View style={styles.part1}>
        <WeightGoalInfo
          currentWeight={currWeight}
          targetWeight={trWeight}
          timeRemaining={weightGoalData.timeRemaining}
          navigation={navigation}
          handleCurrentWeightChange={handleCurrentWeightChange}
          handleTargetWeightChange={handleTargetWeightChange}
        />

        <View style={styles.weightInfo}>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ color: "#323232", paddingBottom: 20 }}>
              Current Weight :
            </Text>
            <View
              style={{
                flexDirection: "row-reverse",

                width: "60%",
              }}
            >
              <Text style={styles.weigthStyle}>{currWeight} Kgs</Text>
            </View>
          </View>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ color: "#323232", paddingBottom: 20 }}>
              Target Weight :
            </Text>
            <View
              style={{
                flexDirection: "row-reverse",

                width: "60%",
              }}
            >
              <Text style={styles.weigthStyle}>{trWeight} Kgs</Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.part2}>
        <Text>Analysis</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    fontFamily: "Roboto",
    width: "100%",
    flex: 1,
    alignItems: "center",

    backgroundColor: "#FFFCFA",
  },
  part1: {
    flex: 2,
    backgroundColor: "#FFFFFF",
    width: "100%",
    alignItems: "flex-start",

    justifyContent: "flex-start",

    elevation: 5,
    shadowColor: "#763400",
  },
  part2: {
    flex: 3,
    //backgroundColor: "#FFEDE4",
    backgroundColor: "#FFFFFF",
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    marginTop: "10%",
    marginBottom: "30%",
    elevation: 10,
    shadowColor: "#000000",
  },
  weightInfo: {
    textDecorationColor: "#323232",
    marginTop: 20,
    marginLeft: "10%",
  },
  weigthStyle: {
    color: "black",
    fontWeight: "500",
    fontSize: 16,
  },
});

export default WeightTrackerScreen;
