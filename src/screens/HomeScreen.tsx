import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import AsyncStorage from "@react-native-community/async-storage";
import SafeAreaView from "react-native-safe-area-view";
import Header from "../components/globals/Header";
import TrackerCard from "../components/trackers/TrackerCardsTab";
import { FOOD_TYPE_SCREEN, LOGIN_SCREEN } from "../constants/ScreenNames";
import CaloriesIntake from "./CaloriesIntake";
import { getToken, getUserIdFromToken } from "../utils/JwtUtil";

interface NavigationProps {
  navigation: any;
}

const HomeScreen = ({ navigation }: NavigationProps) => {
  //  this is done for each user's reminder data as everyone will have different data
  React.useEffect(()=>{
    const getUniqueKeyForAsync = async()=>{
      let token = await getToken();
      console.log("token in home screen for  reminder screen ---- : " + token);
      let userId = getUserIdFromToken(token);
      console.log("User id in home screen for reminder screen is : " + userId);
      let thisKey = 'storedToDoListFor'+userId;
      AsyncStorage.setItem('keyForReminder',thisKey);
      console.log("key in getUniqueKeyForAsync ... ",thisKey);
     }
     const getUniqueKeyForAsyncWaterReminder = async()=>{
          let token = await getToken();
          console.log("token in home screen for  reminder screen for water reminder : " + token);
          let userId = getUserIdFromToken(token);
          console.log("User id in home screen for reminder screen for water reminder is : " + userId);
          let thisKey = 'storedWaterReminder'+userId;
          AsyncStorage.setItem('keyForWaterReminder',thisKey);
          console.log("key in getUniqueKeyForAsyncWaterReminder() ... ",thisKey);
         }
    getUniqueKeyForAsync();
    getUniqueKeyForAsyncWaterReminder();
  },[])

  return (
    <ScrollView>
      <SafeAreaView style={styles.safeAreaContainer}>
        <Header navigation={navigation} />
        <CaloriesIntake navigation={navigation} />
        <View style={{ marginTop: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.navigate(FOOD_TYPE_SCREEN)}
            style={styles.addItems}
          >
            <Text style={styles.addItemsText}>ADD MORE ITEMS {">"}</Text>
          </TouchableOpacity>
        </View>
        <TrackerCard navigation={navigation} />
      </SafeAreaView>
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  safeAreaContainer: {
    marginTop: 50,
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFCFA",
  },
  addItems: {
    paddingTop: 10,
    backgroundColor: "#FFFCFA",
  },
  addItemsText: {
    fontSize: 13,
    color: "#EB6D00",
    textAlign: "right",
    marginLeft: 250,
    fontWeight: "900",
  },
  trackers: {
    alignItems: "center",
    height: 700,
    backgroundColor: "#FFFCFA",
  },
});
