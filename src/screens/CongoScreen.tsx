import React from "react";
import { Text } from "react-native-elements";
import CustomButton from "../components/globals/CustomButton";
import { FOOD_TYPE_SCREEN, HOME_SCREEN } from "../constants/ScreenNames";
import { useDispatch } from "react-redux";
import { resetChecked } from "../store";

const CongoScreen = ({ navigation }: any) => {
  const dispatch = useDispatch();

  const onPressHandler = () => {
    console.log("Button clicked");
    dispatch(resetChecked);
    navigation.navigate(HOME_SCREEN);
  };

  return <CustomButton title={"CONGRATULATIONS"} onPress={onPressHandler} />;
};

export default CongoScreen;
