import React, { useState } from "react";
import { View, Text, TextInput, StyleSheet, Alert } from "react-native";
import CustomButton from "../components/globals/CustomButton";
import ResendOtp from "./ResendOtp";
import axios from "axios";
import { VERIFY_OTP_END_POINT } from "../constants/ApiConstants";
import { AuthServiceApi } from "../apis/FitBuddyApi";
import FitbuddyLogo from "../components/globals/FitbuddyLogo";
interface NavigationProps {
  navigation: any;
  route: any;
}

interface FormErrors {
  usernameError: string;
  otpError: string;
}

const VerifyOtpScreen = ({ navigation, route }: NavigationProps) => {
  const { email } = route.params;

  const [username, setUsername] = useState<string>("");
  const [otp, setOtp] = useState<string>("");

  const [errors, setErrors] = useState<FormErrors>({
    usernameError: "",
    otpError: "",
  });

  const handleValidation = () => {
    let formIsValid = true;
    const updatedErrors: FormErrors = {
      usernameError: "",
      otpError: "",
    };

    if (!username) {
      updatedErrors.usernameError = "UserName is required";
      formIsValid = false;
    }

    if (!otp) {
      updatedErrors.otpError = "OTP is required";
      formIsValid = false;
    } else if (!/^\d{6}$/.test(otp)) {
      updatedErrors.otpError = "Please enter a six-digit number";
      formIsValid = false;
    }

    setErrors(updatedErrors);
    return formIsValid;
  };

  const handleUserNamechange = (text: string) => {
    setUsername(text);
  };

  const handleOtpChange = (text: string) => {
    setOtp(text);
  };

  const validateInput = (input: string): boolean => {
    const regex = /^\d{6}$/;
    return regex.test(input);
  };

  const handleSubmit = () => {
    if (handleValidation()) {
      console.log("verifying otp");
      verifyOtp();
    }
  };

  const data = { username, otp };
  const headers = {
    "content-type": "application/json",
  };
  //call to verify otp api
  const verifyOtp = () => {
    axios;
    AuthServiceApi.post(
      VERIFY_OTP_END_POINT,
      { username, otp },
      { headers: headers }
    )
      .then((response) => {
        // Handle the success response
        console.log(response.data);
        Alert.alert("OTP verified", "Please Login");
        navigation.navigate("Login");
      })
      .catch((error) => {
        if (error.response.status == 404) {
          Alert.alert("Error", error.response.data.message);
        } else if (error.response.status == 406) {
          Alert.alert("Invalid OTP", error.response.data.message);
        } else {
          Alert.alert("Network error, Please try again later");
        }
        // Handle the error response
      });
  };

  return (
    <View style={styles.container}>
      <FitbuddyLogo />
      <View style={styles.text}>
        <Text>Check your email address for otp</Text>
      </View>
      <TextInput
        placeholder="username"
        value={username}
        onChangeText={handleUserNamechange}
        style={styles.input}
      ></TextInput>
      {errors.usernameError ? (
        <Text style={styles.error}>{errors.usernameError}</Text>
      ) : null}

      <TextInput
        keyboardType="numeric"
        placeholder="OTP"
        value={otp}
        onChangeText={handleOtpChange}
        style={styles.input}
      ></TextInput>
      {errors.otpError ? (
        <Text style={styles.error}>{errors.otpError}</Text>
      ) : null}
      <View style={styles.resendButton}>
        <ResendOtp email={email} />
      </View>

      <View style={styles.button}>
        <CustomButton
          title={"CONTINUE"}
          style={styles.button}
          onPress={handleSubmit}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    marginLeft: "10%",
    marginRight: "10%",
    marginBottom: "4%",

    padding: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  button: {
    width: "auto",
    justifyContent: "center",

    marginLeft: "10%",
    marginRight: "10%",
  },
  resendButton: {
    marginBottom: "2%",
    flexDirection: "row-reverse",
  },
  container: {
    height: "100%",
    backgroundColor: "#FFFCFA",
  },
  text: {
    margin: 10,
    marginLeft: "10%",
  },
  error: {
    color: "red",
    marginBottom: 10,
    marginLeft: 50,
  },
});

export default VerifyOtpScreen;
