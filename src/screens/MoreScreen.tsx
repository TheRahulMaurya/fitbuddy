import React, { useState } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback,
} from "react-native";
import { Image, Text } from "react-native-elements";
import { Card } from "@rneui/themed";
//More Screen
interface CardData {
  name: string;
  image: string;
  description: string;
}
const data: CardData[] = [
  {
    name: "Intake Analysis",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/180d7816ab51812365707fae6b15381c/FoodTracking.png",
    description: "Provide analysis report to user about their intake.",
  },
  {
    name: "Schedule Appointment",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/8b1dc78619803be101fc87e62343ff0f/AppoitmentScheduling.webp",
    description: "Schedule Appointment with healthcare providers.",
  },

  {
    name: "Get Personalised Recommendations",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/76f524cadca99b7fd48340acab4e0cb6/personalisedRecommendations.jpg",
    description:
      "Get personalised recommendations for effective health management.",
  },
  {
    name: "User Engagement",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/d6b4ec22c1b27188c77f50501915e249/UserEngagement.jpg",
    description: "Follow other users and share goals and plans.",
  },
  {
    name: "Exercise Tracker",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/e086eaffaf0be0d16b85b4501cd8d001/ExerciseTracker.jpg",
    description: "Track your daily exercises.",
  },
  {
    name: "Steps Tracker",
    image:
      "https://gitlab.com/kritisonker95/fitbuddy/uploads/80a4b5283d1d7f4f330c2d32854868df/StepsTracker.png",
    description: "Track your daily steps.",
  },
];
const MoreScreen: React.FC = () => {
  const [selectedCard, setSelectedCard] = useState<CardData | null>(null);
  const renderItem = ({ item }: { item: CardData }) => (
    <TouchableWithoutFeedback onPress={() => setSelectedCard(item)}>
      <View>
        <Card containerStyle={styles.cardContainer}>
          <Image source={{ uri: item.image }} style={styles.cardImage} />
          <View style={styles.titleContainer}>
            <Text style={styles.cardTitle}>{item.name}</Text>
          </View>
        </Card>
      </View>
    </TouchableWithoutFeedback>
  );
  const renderModal = () => (
    <Modal visible={selectedCard !== null} transparent animationType="fade">
      <View style={styles.modalContainer}>
        {selectedCard && (
          <Card containerStyle={styles.modalCardContainer}>
            <Image
              source={{ uri: selectedCard.image }}
              style={styles.modalCardImage}
            />
            <Text h4>{selectedCard.name}</Text>
            <Text>{selectedCard.description}</Text>
            <TouchableWithoutFeedback onPress={() => setSelectedCard(null)}>
              <Text style={styles.closeButton}>Close</Text>
            </TouchableWithoutFeedback>
          </Card>
        )}
      </View>
    </Modal>
  );
  return (
    <View style={styles.container}>
      <Card containerStyle={styles.comingSoonContainer}>
        <Text style={styles.text}>Coming Soon</Text>
      </Card>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}
      />
      {renderModal()}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  cardContainer: {
    flex: 1,
    margin: 8,
    width: 180,
  },
  cardImage: {
    width: 150,
    height: 210,
    flex: 1,
    marginTop: -32,
    resizeMode: "contain",
  },
  text: {
    color: "red",
    fontSize: 24,
    fontWeight: "bold",
  },
  comingSoonContainer: {
    height: 80,
    width: 368,
    marginLeft: 10,
    alignItems: "center",
    backgroundColor: "black",
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalCardContainer: {
    width: "80%",
    backgroundColor: "#FFF",
    padding: 16,
  },
  modalCardImage: {
    width: "100%",
    height: 200,
    resizeMode: "contain",
  },
  closeButton: {
    marginTop: 16,
    textAlign: "center",
    color: "grey",
    textDecorationLine: "underline",

    borderColor: "blue",
  },
  titleContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "pink",
    padding: 8,
  },
  cardTitle: {
    color: "blue",
    fontWeight: "bold",
  },
});
export default MoreScreen;
