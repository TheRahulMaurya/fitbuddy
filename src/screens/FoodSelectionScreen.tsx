import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import RecommendItems from "../components/RecommendItems";
import Search from "../components/Search";
import CustomButton from "../components/globals/CustomButton";
import { FETCH_ALL_FOOD_END_POINT } from "../constants/ApiConstants";
import { GOALS_SCREEN } from "../constants/ScreenNames";
import { mealTypeMapper } from "../mapper";
import { fetchItems, selectFoodCart, selectedFoodType } from "../store";
import { saveFoodIntake } from "../store/thunks/FoodIntakeThunk";

const FoodSelectionScreen = ({ navigation }) => {
  const [searchText, setSearchText] = useState(null);
  const dispatch = useDispatch();
  const type = useSelector(selectedFoodType);
  const foodCart = useSelector(selectFoodCart);

  const onPressHandler = () => {
    console.log("Button clicked");
    const intake = {
      userId: 1,
      mealTypeId: type.mealTypeId,
      userFoodIntakeItems: foodCart,
    };
    dispatch(saveFoodIntake(intake));
    setTimeout(() => {
      console.log("inside timeout");
      navigation.navigate(GOALS_SCREEN);
    }, 500);
  };

  useEffect(() => {
    dispatch(
      fetchItems(FETCH_ALL_FOOD_END_POINT + "?mealType=" + type.mealTypeId)
    );
  }, []);
  return (
    <View>
      <Search searchText={searchText} setSearchText={setSearchText} />
      <View style={styles.itemsView}>
        <RecommendItems navigation={navigation} searchedText={searchText} />
      </View>
      {foodCart.length >= 1 ? (
        <View style={styles.button}>
          <CustomButton
            title={`TRACK FOR ${mealTypeMapper(type.mealType).toUpperCase()}`}
            onPress={onPressHandler}
          />
        </View>
      ) : (
        <></>
      )}
    </View>
  );
};

export default FoodSelectionScreen;

const styles = StyleSheet.create({
  button: {
    margin: "4%",
  },
  itemsView: {
    height: "77%",
  },
});
