import React from "react";
import {View,Text,Pressable} from "react-native";

const VerifyEmailScreen : React.FC = ({navigation}:any) => {
    return(
        <View  className="flex flex-1 justify-center flex items-center flex flex-col space-y-8">
            <Text> In Verify Email Screen</Text>
            <Pressable onPress={() => navigation.navigate("Login")}>
                    <View className='flex items-center flex justify-center bg-orange-400 rounded-lg h-12 w-60 '>
                    <Text className='text-white text-base font-semibold leading-5'>Back to Login</Text>
                    </View>
            </Pressable>
        </View>
    )
}

export default VerifyEmailScreen;