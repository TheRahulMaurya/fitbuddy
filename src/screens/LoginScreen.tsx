import React, { useRef, useState } from "react";
import {
  View,
  Text,
  TextInput,
  Pressable,
  TouchableOpacity,
  Keyboard,
  Alert,
} from "react-native";
import axios from "axios";
import { SafeAreaView } from "react-native-safe-area-context";
import { Ionicons } from "@expo/vector-icons";
import jwt_decode from "jwt-decode";

import FitbuddyLogo from "../components/globals/FitbuddyLogo";
import Loader from "../components/globals/Loader";
import AsyncStorageAccess from "../components/globals/AsyncStorageAccess";
import { AuthServiceApi } from "../apis/FitBuddyApi";
import { LOGIN_END_POINT } from "../constants/ApiConstants";

interface FormData {
  username: string;
  password: string;
}

interface Data {
  token: string;
  type: string;
}

const defaultData = {} as Data;

const LoginScreen: React.FC = ({ navigation }: any) => {
  const [response, setResponse]: [Data, (response: Data) => void] =
    React.useState(defaultData); //to get the response
  const [username, setUsername]: [
    FormData["username"],
    (username: string) => void
  ] = useState("");
  const [password, setPassword]: [
    FormData["password"],
    (password: string) => void
  ] = useState("");
  const [loading, setLoading]: [boolean, (loading: boolean) => void] =
    useState<boolean>(false);
  const [errortext, setErrortext]: [string, (errortext: string) => void] =
    useState("");
  const [invalid, setInvalid] = useState(false);

  const passwordInputRef = useRef<TextInput>(null);

  const [showPassword, setShowPassword] = useState(false);

  const handleTogglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const validatePassword = (input: string): boolean => {
    const regex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=!])(?!.*\s).{8,30}$/;
    return regex.test(input);
  };

  const handleSubmit = async () => {
    console.log("Called handlesubmit");
    setErrortext("");

    if (!username) {
      alert("Please fill Username");
      return;
    }

    if (!password) {
      alert("Please fill Password");
      return;
    }

    if (!validatePassword(password)) {
      Alert.alert(
        "Invalid password",
        "Please enter valid password Requirement : Minimum 8 characters,one Uppercase , one lowercase , one digit and one special character"
      );
      return;
    }

    setLoading(true);

    const credentials = {
      username,
      password,
    };

    await AuthServiceApi.post(LOGIN_END_POINT, credentials)
      .then((response) => {
        setLoading(false);
        setUsername("");
        setPassword("");
        setInvalid(false);
        if (response.status == 200) {
          AsyncStorageAccess.storeUserCredential(
            JSON.stringify(response.data.data)
          );
          console.log("response.data");
          console.log(response.data);
          console.log(response.data.data.token);
          navigation.navigate("Footer");
        }
      })
      .catch((error) => {
        //Hide Loader
        setLoading(false);
        if (error.response.status == 406) {
          setErrortext(error.response.message);
          console.log(errortext);
          navigation.navigate("SendOtp", {
            nextScreen: "VerifyOtp",
          });
        } else if (error.response.status == 401) {
          setErrortext(error.response.message);
          alert(error.response.data.message);
          setInvalid(true);
          return;
        }
      });
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#FFFCFA" }}>
      <View className="flex">
        <Loader loading={loading} />

        <View>
          <FitbuddyLogo />
        </View>

        <View className=" flex flex-col space-y-8 justify-center flex items-center mt-28 mx-10">
          <View className="flex-auto box-border h-16 w-80  border-2 bg-white rounded-2xl border-slate-400 px-3.5 py-3.5">
            <TextInput
              className="flex-1 p-2"
              placeholder="Username"
              value={username}
              onChangeText={(Username) => setUsername(Username)}
              returnKeyType="next"
              onSubmitEditing={() =>
                passwordInputRef.current && passwordInputRef.current.focus()
              }
              blurOnSubmit={false}
            />
          </View>
          <View
            className="flex-auto box-border h-16 w-80  border-2 bg-white rounded-2xl border-slate-400 px-3.5 py-3.5"
            style={{ flexDirection: "row" }}
          >
            <TextInput
              className="flex-1 p-2"
              placeholder="Password"
              value={password}
              onChangeText={(Password) => setPassword(Password)}
              secureTextEntry={!showPassword}
              ref={passwordInputRef}
              onSubmitEditing={Keyboard.dismiss}
              blurOnSubmit={false}
            />
            <Ionicons
              name={showPassword ? "eye-off" : "eye"}
              size={24}
              color="#777"
              className="mx-2"
              onPress={handleTogglePasswordVisibility}
            />
          </View>
        </View>

        <View className="flex flex-row-reverse mx-12">
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("SendOtp", {
                nextScreen: "ForgotPassword",
              })
            }
          >
            <Text className="mt-2.5 text-red-400">Forgot Password?</Text>
          </TouchableOpacity>
        </View>

        <View className="flex flex-col flex items-center flex justify-center space-y-8 mt-24">
          <Pressable onPress={handleSubmit}>
            <View
              style={{ backgroundColor: "#EB6D00" }}
              className="flex items-center flex justify-center bg-orange-400 rounded-lg h-12 w-60 "
            >
              <Text className="text-white text-base font-semibold leading-5">
                Continue
              </Text>
            </View>
          </Pressable>
          <View style={{ flexDirection: "row" }}>
            <Text>Not registered yet ? </Text>
            <Pressable onPress={() => navigation.navigate("UserRegistration")}>
              <Text className="text-red-400">Create Account</Text>
            </Pressable>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;
