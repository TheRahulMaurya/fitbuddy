import { MaterialCommunityIcons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-community/async-storage";
import { useIsFocused } from "@react-navigation/native";
import { Card } from "@rneui/themed";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import CircularProgress from "react-native-circular-progress-indicator";
import * as Progress from "react-native-progress";
import { FitBuddyApi } from "../apis/FitBuddyApi";
import { FETCH_USER_FOOD_INTAKE_FOR_DAY_END_POINT } from "../constants/ApiConstants";
import { getToken, getUserIdFromToken } from "../utils/JwtUtil";

const { width } = Dimensions.get("screen");
const cardWidth = width * 0.95;

interface NavigationProps {
  navigation: any;
}

export default function CaloriesIntake({ navigation }: NavigationProps) {
  const [calorie, setCalorie] = useState<number>(0);
  const [protein, setProtien] = useState<number>(0);
  const [fat, setFat] = useState<number>(0);
  const [carbs, setCarbs] = useState<number>(0);
  const [fibre, setFibre] = useState<number>(0);
  const isFocused = useIsFocused();

  let calPer = 0;
  let protPer = 0;
  let fatPer = 0;
  let carbsPer = 0;
  let fibrePer = 0;

  const getAllData = async () => {
    let cal = 0;
    let prot = 0;
    let fats = 0;
    let carb = 0;
    let fib = 0;

    let currentDate = moment().format("DD-MM-YYYY");
    let token = await getToken();
    console.log("token---- : " + token);
    let userId = getUserIdFromToken(token);
    console.log("User id is : " + userId);

    const config = {
      headers: {
        Date: '05-06-2023',
        Authorization: "Bearer " + token,
      },
    };

    FitBuddyApi.get(FETCH_USER_FOOD_INTAKE_FOR_DAY_END_POINT + userId, config)
      .then((response) => {
        const allData = response.data.data;
        allData.map(({ userFoodIntakeItems }: any) => {
          userFoodIntakeItems.map((item: any) => {
            {
              Object.keys(item).map((key) => {
                if (key === "foodPropMaps") {
                  const foodProp = item[key];
                  {
                    Object.keys(foodProp).map((key) => {
                      if (key === "foodMacros") {
                        const macros = foodProp[key];
                        console.log(macros);
                        {
                          Object.keys(macros).map((key) => {
                            if (key === "calories") {
                              cal += item.quantity * macros[key];
                            }
                            if (key === "protein") {
                              prot += item.quantity * macros[key];
                            }
                            if (key === "carbs") {
                              carb += item.quantity * macros[key];
                            }
                            if (key === "fiber") {
                              fib += item.quantity * macros[key];
                            }
                            if (key === "fats") {
                              fats += item.quantity * macros[key];
                            }
                          });
                        }
                      }
                    });
                  }
                }
              });
            }
          });
        });

        console.log("Calorie " + cal);
        console.log("Protien " + prot);
        console.log("Fats " + fats);
        console.log("Carbs " + carb);
        console.log("Fibre " + fib);

        setCalorie(cal);
        setCarbs(carb);
        setFat(fats);
        setFibre(fib);
        setProtien(prot);
      })
      .catch((error) => {
        console.error("Load axios get call error: ", error);
        // AsyncStorage.clear();
        AsyncStorage.removeItem("userCred");
        navigation.navigate("Login");
      });
  };

  useEffect(() => {
    console.log("----------inside useEffect 2----------------");
    // Call only when screen open or when back on screen
    if (isFocused) {
      console.log("----------inside useEffect 2 if----------------");
      getAllData();
    }
  }, [navigation, isFocused]);

  console.log("Calorie State " + calorie);
  console.log("Protien State " + protein);
  console.log("Fats State " + fat);
  console.log("Carbs State " + carbs);
  console.log("Fibre State " + fibre);

  calPer = Math.ceil((calorie / 2500) * 100);
  protPer = Math.ceil((protein / 162) * 100);
  fatPer = Math.ceil((fat / 100) * 100);
  carbsPer = Math.ceil((carbs / 200) * 100);
  fibrePer = Math.ceil((fibre / 72) * 100);

  return (
    <View style={{ backgroundColor: "#FFFCFA" }}>
      <Card containerStyle={styles.container}>
        <View style={styles.calorie}>
          <View style={{ paddingHorizontal: 10 }}>
            <CircularProgress
              value={calPer}
              radius={45}
              progressValueColor={"black"}
              maxValue={100}
              titleStyle={{ fontWeight: "bold" }}
              activeStrokeColor={calPer > 100 ? "#f53131" : "#EB6D00"}
              valueSuffix="%"
            />
            <MaterialCommunityIcons
              name="silverware-fork-knife"
              color={"#e65c0b"}
              size={20}
              style={styles.icon}
            />
          </View>
          <View>
            <Text style={styles.totalCalories}>{calorie} of 2500</Text>
            <Text style={styles.caloriesConsumed}>Calories Consumed</Text>
          </View>
        </View>

        <Card.Divider style={styles.divider} />

        <View>
          <View style={styles.card}>
            <View style={{ paddingHorizontal: 10 }}>
              <Text style={styles.calorieIntake}>Proteins : {protPer}%</Text>
              <Progress.Bar
                progress={protPer / 100}
                width={150}
                color={protPer > 100 ? "#f53131" : "#5ccf4e"}
                borderColor={"#d6d6c2"}
                unfilledColor={"#d6d6c2"}
              />
            </View>
            <View>
              <Text style={styles.calorieIntake}>Fats : {fatPer}%</Text>
              <Progress.Bar
                progress={fatPer / 100}
                width={150}
                color={fatPer > 100 ? "#f53131" : "#5ccf4e"}
                borderColor={"#d6d6c2"}
                unfilledColor={"#d6d6c2"}
              />
            </View>
          </View>
          <View style={styles.card}>
            <View style={{ paddingHorizontal: 10 }}>
              <Text style={styles.calorieIntake}>Carbs : {carbsPer}%</Text>
              <Progress.Bar
                progress={carbsPer / 100}
                width={150}
                color={carbsPer > 100 ? "#f53131" : "#5ccf4e"}
                borderColor={"#d6d6c2"}
                unfilledColor={"#d6d6c2"}
              />
            </View>
            <View>
              <Text style={styles.calorieIntake}>Fibre : {fibrePer}%</Text>
              <Progress.Bar
                progress={fibrePer / 100}
                width={150}
                color={fibrePer > 100 ? "#f53131" : "#5ccf4e"}
                borderColor={"#d6d6c2"}
                unfilledColor={"#d6d6c2"}
              />
            </View>
          </View>
        </View>
      </Card>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: cardWidth,
    borderRadius: 15,
    elevation: 5,
  },
  calorie: {
    flexDirection: "row",
  },
  icon: {
    position: "absolute",
    paddingLeft: 45,
    paddingTop: 55,
  },
  totalCalories: {
    marginBottom: 9,
    marginTop: 25,
    fontWeight: "bold",
  },
  caloriesConsumed: {
    marginBottom: 9,
    paddingBottom: 4,
  },
  divider: {
    marginTop: 10,
  },
  card: {
    flexDirection: "row",
    height: 50,
  },
  calorieIntake: {
    marginBottom: 9,
    marginTop: 10,
  },
});
