import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";

type GoalInfoProps = {
  currentWeight: Number;
  targetWeight: Number;
  timeRemaining: Number;
  navigation: any;
  handleCurrentWeightChange: (currentWeight: number) => void;
  handleTargetWeightChange: (targetWeight: number) => void;
};

const WeightGoalInfo = ({
  currentWeight,
  targetWeight,
  timeRemaining,
  navigation,
  handleCurrentWeightChange,
  handleTargetWeightChange,
}: GoalInfoProps) => {
  //send props as js object
  const handleEdit = () => {
    navigation.navigate("WeightGoals", {
      cw: currentWeight,
      tw: targetWeight,
      navigation: navigation,
      hcwc: handleCurrentWeightChange,
      htwc: handleTargetWeightChange,
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.circle}>
        <Image
          source={{
            uri: "https://gitlab.com/makarandlad2/fitbuddy/uploads/231794d49e506b41a1bb690108c09cbc/weightGoalImage.png",
          }}
          style={{ width: 50, height: 50, borderRadius: 25 }}
        />
      </View>
      <View style={{ flexDirection: "column" }}>
        <Text style={styles.text}>
          Lose {parseFloat(currentWeight - targetWeight).toFixed(1)} Kgs
        </Text>
        <Text style={{ fontSize: 14, color: "grey" }}>
          {timeRemaining} weeks remaining
        </Text>
      </View>
      <View
        style={{
          flexDirection: "row-reverse",

          width: "30%",
        }}
      >
        <FontAwesome5
          solid
          name="edit"
          size={20}
          color="black"
          onPress={handleEdit}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
    marginTop: 5,
    marginLeft: "10%",
    flexDirection: "row",
    alignItems: "center",
  },
  circle: {
    width: 50,

    borderRadius: 25,
    backgroundColor: "#ccc",
    marginRight: 10,
  },
  text: {
    fontSize: 18,
  },
});

export default WeightGoalInfo;
