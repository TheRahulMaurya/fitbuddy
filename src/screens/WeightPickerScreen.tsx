// import React, { useState } from "react";
// import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
// import ScrollPicker from "react-native-wheel-scrollview-picker";

// interface WeightPickerProps {
//   onValueSelected: (selectedValue: number) => void;
// }

// const WeightPicker: React.FC<WeightPickerProps> = ({ onValueSelected }) => {
//   const [selectedValue1, setSelectedValue1] = useState(1);
//   const [selectedValue2, setSelectedValue2] = useState(0);

//   const handleValueChange1 = (value: string | number) => {
//     setSelectedValue1(Number(value));
//   };
//   const handleValueChange2 = (value: string | number) => {
//     setSelectedValue2(Number(value));
//   };

//   const handleDonePress = () => {
//     const weight = parseFloat(`${selectedValue1}.${selectedValue2}`);
//     onValueSelected(weight);
//   };

//   const values1 = Array.from({ length: 250 }, (_, i) => (i + 1).toString());
//   const values2 = Array.from({ length: 10 }, (_, i) => i.toString());

//   return (
//     <View style={styles.container}>
//       <View style={styles.part1}></View>
//       <View style={styles.pickerContainer}>
//         <ScrollPicker
//           dataSource={values1}
//           selectedIndex={selectedValue1 - 1}
//           renderItem={(data) => <Text style={styles.pickerItem}>{data}</Text>}
//           onValueChange={handleValueChange1}
//           wrapperHeight={120}
//           style={{ backgroundColor: "#E5E5E5" }}
//           highlightColor="#EEEEEE"
//         />
//         <Text
//           style={{
//             fontSize: 20,
//             fontWeight: "300",
//             backgroundColor: "#E5E5E5",
//           }}
//         >
//           .
//         </Text>
//         <ScrollPicker
//           dataSource={values2}
//           selectedIndex={selectedValue2 - 1}
//           renderItem={(data) => <Text style={styles.pickerItem}>{data}</Text>}
//           onValueChange={handleValueChange2}
//           wrapperHeight={120}
//         />
//       </View>
//       <View style={styles.header}>
//         <TouchableOpacity onPress={handleDonePress}>
//           <Text style={styles.doneText}>Done</Text>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#F5FCFF",
//   },
//   part1: {
//     flex: 2,
//     backgroundColor: "blue",
//     width: "100%",
//   },
//   part2: {
//     flex: 2,
//     backgroundColor: "#FFFFFF",
//     width: "100%",
//   },

//   header: {
//     height: 50,
//     backgroundColor: "#FFFFFF",
//     alignItems: "center",
//     justifyContent: "center",
//     paddingRight: 20,
//   },
//   doneText: {
//     color: "#007AFF",
//     fontSize: 18,
//     fontWeight: "bold",
//   },
//   pickerContainer: {
//     borderRadius: 30,
//     alignItems: "center",
//     justifyContent: "center",
//     flexDirection: "row",
//     flex: 2,
//     backgroundColor: "#E5E5E5",
//     width: "100%",
//   },
//   pickerItem: {
//     color: "#333333",
//     fontSize: 16,
//   },
// });

// export default WeightPicker;

import React, { useState } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import SmoothPicker from "react-native-smooth-picker";
import { AntDesign } from "@expo/vector-icons";
import { useTailwind } from "tailwind-rn/dist";

interface NavigationProps {
  navigation: any;
  route: any;
}
const WeightPicker = ({ navigation, route }: NavigationProps) => {
  const tww = useTailwind();
  const { weight, handleChange } = route.params;

  const weightStringArr = weight.toString().split(".");

  console.log("weight Array" + weightStringArr);

  const [firstValue, setFirstValue] = useState<number>(
    parseInt(weightStringArr[0])
  );
  const [secondValue, setSecondValue] = useState<number>(
    parseInt(weightStringArr[1])
  );

  const firstData: number[] = Array.from({ length: 250 }, (_, i) => i + 1);
  const secondData: number[] = Array.from({ length: 10 }, (_, i) => i);

  const handleFirstChange = (value: number) => {
    setFirstValue(value);
  };

  const handleSecondChange = (value: number) => {
    setSecondValue(value);
  };

  const handleSubmit = () => {
    console.log(`Selected values: ${firstValue}.${secondValue}`);
    const pickerWeight = parseFloat(`${firstValue}.${secondValue}`);
    handleChange(pickerWeight);
    navigation.goBack();
  };

  const onClose = () => {
    navigation.goBack();
  };

  return (
    <View
      style={{
        ...tww("flex-auto flex-col-reverse h-full w-full mb-0"),
        backgroundColor: "#ACACAC",
      }}
    >
      <View style={tww("flex-col-reverse h-2/3")}>
        <View
          style={{
            marginBottom: "5%",
            height: "60%",
            backgroundColor: "white",
            borderRadius: 30,
          }}
        >
          <Text
            style={{
              marginTop: "2%",
              textAlign: "center",
              fontSize: 14,
              fontWeight: "300",
            }}
          >
            Select Weight in Kgs
          </Text>
          <View style={{ height: "75%" }}>
            <View style={styles.container}>
              <View style={styles.pickerContainer}>
                <SmoothPicker
                  data={firstData}
                  onSelected={({ item }) => handleFirstChange(item)}
                  activeIndex={firstValue - 1}
                  scrollAnimation
                  initialScrollToIndex={firstValue - 1}
                  selectOnPress
                  magnet
                  snapInterval={40}
                  style={{ height: 150 }}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item, index }) => {
                    const focused = firstValue === item;
                    const itemStyle = focused
                      ? styles.focusedText
                      : styles.text;
                    const itemBlur = focused ? 10 : 5;
                    return (
                      <Text style={[itemStyle, { textShadowRadius: itemBlur }]}>
                        {item}
                      </Text>
                    );
                  }}
                />
                <Text style={styles.dot}>.</Text>
                <SmoothPicker
                  data={secondData}
                  onSelected={({ item }) => handleSecondChange(item)}
                  activeIndex={secondValue}
                  magnet
                  scrollAnimation
                  initialScrollToIndex={secondValue}
                  selectOnPress
                  snapInterval={40}
                  style={{ height: 150 }}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item, index }) => {
                    const focused = secondValue === item;
                    const itemStyle = focused
                      ? styles.focusedText
                      : styles.text;
                    const itemBlur = focused ? 10 : 5;
                    return (
                      <Text style={[itemStyle, { textShadowRadius: itemBlur }]}>
                        {item < 10 ? `0${item}` : item}
                      </Text>
                    );
                  }}
                />
              </View>
            </View>
          </View>
          <View
            style={{
              alignItems: "center",
              borderTopColor: "black",
              borderTopWidth: 0.5,
            }}
          >
            <TouchableOpacity onPress={handleSubmit}>
              <Text style={{ fontSize: 18, padding: 5, color: "blue" }}>
                Done
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  pickerContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 16,
    color: "#222",
    textAlign: "center",
    padding: 20,
  },
  focusedText: {
    fontSize: 24,
    color: "blue",
    textAlign: "center",
    padding: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(0, 0, 0, 0.1)",
    textShadowOffset: { width: 0, height: 2 },
  },
  dot: {
    fontSize: 24,
    color: "#222",
    textAlign: "center",
    padding: 20,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "blue",
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
  },
});

export default WeightPicker;
