import jwt_decode from "jwt-decode";
import AsyncStorageAccess from "../components/globals/AsyncStorageAccess";

export const getToken = async () => {
  const token = await AsyncStorageAccess.getUserCred(); //note here , not making some of them as state because of async calls and re-rendering - sync not between them
  return token;
};

export const getUserIdFromToken = (token) => {
  const decoded = jwt_decode(token);
  return decoded.user.userId;
};

export const getUserFromToken = (token) => {
  const decoded = jwt_decode(token);
  return decoded.user;
};
