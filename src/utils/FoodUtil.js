import { mealPropMapper } from "../mapper";

export const getFoodDetail = (food, quantity, selectedPropIndex) => {
  return `${
    quantity * food.foodPropMaps[selectedPropIndex].foodMacros.calories
  } Calories | ${quantity} ${mealPropMapper(
    food.foodPropMaps[selectedPropIndex].foodProportion.proportion
  )} ${food.foodName.toLowerCase()} | ${`${
    food.foodPropMaps[selectedPropIndex].foodMacros.weight !== 0
      ? `${quantity * food.foodPropMaps[selectedPropIndex].foodMacros.weight}g`
      : `${quantity * food.foodPropMaps[selectedPropIndex].foodMacros.volume}ml`
  }`}`;
};
