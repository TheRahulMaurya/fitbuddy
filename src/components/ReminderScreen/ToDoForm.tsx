import React from "react";
import { View ,Text,TextInput,Keyboard,TouchableOpacity} from "react-native";
import { TimePicker } from "react-native-simple-time-picker";
/**
 * This is a child component (functional) or can be considered as an independent component , it a form to take up 
 * activity details forwhich reminders need to be set up
 * @param addToDo - a function passed as props from parent component - index.tsx of Reminder to child component ToDoForm
 * @returns a component that has one input box to take activity name of type string/text , 3 inputs of type 
 * TimePicker to pick hour,minute and am/pm , a text showing the time selected and an Add button to add 
 * the activity to be reminded of 
 */


const TodoForm= ({addTodo}:any) => {
   
    const [activity, setActivity] = React.useState(''); // to store activity name
    const [hours, setHours] = React.useState(0); //to store hour of time
    const [minutes, setMinutes] = React.useState(0); // to store minute of time
    const [ampm,setAmPm]=React.useState('am'); // to store am or pm by default am
    const [time,setTime]=React.useState(''); // to store above values as a single string depiciting time
    
    
    // to set above values when user inputs them
    const handleChange = (value: { hours: number, minutes: number ,ampm: string}) => {
        setHours(value.hours);
        setMinutes(value.minutes);
        setAmPm(value.ampm);
        setTime(value.hours+' : '+value.minutes+' '+value.ampm);
    };

    const handleSubmit = () => {
        Keyboard.dismiss();
        console.log("clicked Add button of ToDoForm")
        addTodo({activity,hours,minutes,ampm,time});
        setActivity("");
        setHours(0);
        setMinutes(0);
        setAmPm('am');
        setTime('');
    };


    
    return (
        <View style={{justifyContent:'space-evenly',alignItems:'center'}}>
            <TextInput
                style={
                    {
                        borderColor: '#CCCCCC',
                        borderTopWidth: 1,
                        borderBottomWidth: 1,
                        height: 45,
                        fontSize: 15,
                        paddingLeft: 20,
                        paddingRight: 20,
                        borderRadius:20
                    }
                }
                placeholder="Add Activity"
                onChangeText={text => setActivity(text)}
                defaultValue={activity}
                onBlur={Keyboard.dismiss}
            />
            <TimePicker value={{ hours, minutes ,ampm}} onChange={handleChange} isAmpm/>
            <Text 
                style={{
                    fontWeight:'bold',
                    fontFamily:''
                }}
            >
                Selected Time: {hours}:{minutes}:{ampm}
            </Text>
            <TouchableOpacity
                style={{
                    borderWidth: 1,
                    borderColor:'#CCCCCC',
                    backgroundColor: '#6fa67c',
                    borderRadius:20,
                    height: 50,
                    paddingLeft: 20,
                    paddingRight: 20,
                    padding: 10,
                    marginTop:5,
                    width:150
                }}
                onPress={handleSubmit}
            >
                <Text style={{
                        color: '#FFFFFF',
                        fontSize: 20,
                        textAlign: 'center',
                    }}>Add
                </Text>
            </TouchableOpacity>
</View>
    );
  }

export default TodoForm;