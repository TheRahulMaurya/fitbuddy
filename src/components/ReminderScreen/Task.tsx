import React from "react";
import { View ,Text,TouchableOpacity,Switch,Button} from "react-native";
import {MaterialCommunityIcons} from '@expo/vector-icons';
import AsyncStorage from "@react-native-community/async-storage";
import { Platform } from "react-native";
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';

//functional component for each activity , it has - Activity Name , Time , switch for enabling and disabling
//the reminder , edit icon , trash icon

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: true,
    }),
  });
  
const Task=({activity,hours,minutes,ampm,time,index,removeTodo}:any)=>{
  
      const[notifid,setNotifid]=React.useState('');//for notification or reminder
       
      const[notification,setNotification]=React.useState(false);
      const notificationListener=React.useRef();
      const responseListener=React.useRef();

      React.useEffect(()=>{
        // to get permission to send the notification to the device
        const getPermission = async () => {
          console.log('Taking Permission to send push notification'); 
          if (Constants.isDevice) {
            const { status: existingStatus } = await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
              const { status } = await Notifications.requestPermissionsAsync();
              finalStatus = status;
            }
            if (finalStatus !== 'granted') {
              alert('Enable push notifications to use the app!');
              await AsyncStorage.setItem('expopushtoken', "");
              return;
            }
            const token = (await Notifications.getExpoPushTokenAsync()).data;
            await AsyncStorage.setItem('expopushtoken', token);
        } else {
          alert('Must use physical device for Push Notifications');
        }
  
          //This code is needed for Android to work
          if ( Platform.OS === 'android' ) {
            Notifications.setNotificationChannelAsync( 'default', {
              name: 'default',
              importance: Notifications.AndroidImportance.MAX,
              vibrationPattern: [0, 250, 250, 250],
              lightColor: '#FF231F7C',
            } );
          }
          console.log('Took Permission to send push notification')
          
      }

        getPermission();

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
          setNotification(notification);
        });

        responseListener.current = Notifications.addNotificationResponseReceivedListener(response=>{});

        return () => {
          Notifications.removeNotificationSubscription(notificationListener.current);
          Notifications.removeNotificationSubscription(responseListener.current);
        };

      },[])
    

    const scheduleDailyNotifications = async (activity:any,hours:any,minutes:any,ampm:any) => {

                  console.log('Scheduling daily notifications');
                  let notifHour=hours;
                  
                  if(hours <=12 && minutes<=59){
                    if((ampm==='pm') 
                        && (hours != 0) && (hours != 12))
                      {
                          notifHour = hours + 12;
                      }
                      if((ampm==='am') && (hours == 12))
                      {
                          notifHour = 0;
                      }
                  }

                  const idOfNotification = await Notifications.scheduleNotificationAsync( {
                    content: {
                      title: "Hey There !",
                      body: "Its time for your "+activity,
                      sound: 'default'
                    },
                    trigger: {
                      hour: notifHour,
                      minute:minutes,
                      repeats:true
                    },
      
                    
              } );
  
              setNotifid(idOfNotification);
              {console.log("Scheduled notification for "+idOfNotification+" at "+notifHour+" : "+minutes);  
              {console.log("notifId is ",idOfNotification)}  ;  
          }
          
      }

      const handleRemoval=()=>{
        disableNotification(notifid);
        removeTodo(index);
      }
      

      React.useEffect(()=>{
        scheduleDailyNotifications(activity,hours,minutes,ampm);
      },[])

      
  
      const disableNotification=(notifId)=>{
         Notifications.cancelScheduledNotificationAsync(notifId);
         {console.log('Disabled Notification of ',notifId)}
      }
  

  
    return(
          <View style={
              {
                  backgroundColor:'white',
                  marginHorizontal:16,
                  marginVertical:4,
                  borderRadius:20,
                  paddingVertical:10,
                  paddingHorizontal:24,
                  flexDirection:'row',
                  alignItems:'center',
                  justifyContent:'space-between'
              }
          }>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                  <View style={{paddingLeft:10}}>
                      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                      <Text style={{fontSize:16}}>{activity}</Text>
                      </View>
                      <Text style={{fontSize:15,color:'green'}}>{time}</Text>
                  </View>
              </View>
              <View style={{flexDirection:'row'}}>
                  <MaterialCommunityIcons name='pencil' size={25} style={{color:'#075e0e'}}/>
                  <TouchableOpacity>
                      <MaterialCommunityIcons name='trash-can' size={25} onPress={handleRemoval} style={{color:'#99050a',marginLeft:5}}/>
                  </TouchableOpacity>
              </View>
          </View>
      )
  };

  export default Task;