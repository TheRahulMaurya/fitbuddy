import React, { useState, useEffect } from 'react';
import { View, Text,Alert, TouchableOpacity } from 'react-native';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import AsyncStorage from "@react-native-community/async-storage";
import { TimePicker } from "react-native-simple-time-picker";

// Request permission for push notifications
const registerForPushNotificationsAsync = async () => {
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      Alert.alert('Error', 'Failed to get push token for notifications!');
      return;
    }
  } else {
    Alert.alert('Error', 'Push notifications are not supported on this device!');
    return;
  }
};

const WaterReminder = () => {

  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [notificationInterval, setNotificationInterval] = useState('');
  const [expoPushToken, setExpoPushToken] = useState<string | null>(null);
  const [waterKey,setWaterKey]=React.useState('');

  useEffect(() => {
    console.log('Rendered Water Reminder');
    registerForPushNotificationsAsync().then((token) => setExpoPushToken(token));
    loadReminderInfo();
  }, []);

  const handleChange = (value: { hours: number, minutes: number}) => {
    setHours(value.hours);
    setMinutes(value.minutes);
};

  const saveReminderInfo = async (interval: string) => {
    try {
      await AsyncStorage.setItem(waterKey, interval);
    } catch (error) {
      Alert.alert('Error', 'Failed to save reminder info!');
    }
  };

  const loadReminderInfo = async () => {
    try {
        const res=await AsyncStorage.getItem('keyForWaterReminder');
        if(res!=null){
            console.log('came inside new way of getting key');
            setWaterKey(res);
            console.log(res);
        }
      const interval = await AsyncStorage.getItem(res);
      if (interval) {
        console.log('time for reminder ',interval);
        setNotificationInterval(interval);
      }
    } catch (error) {
      Alert.alert('Error', 'Failed to load reminder info!');
    }
  };

  const handleSetReminder = () => {

    let totalMinutes=(hours*60)+minutes;
    setNotificationInterval(JSON.stringify(totalMinutes));
    saveReminderInfo(JSON.stringify(totalMinutes));

    const schedulingOptions = {
      content: {
        title: 'Water Reminder',
        body: 'Time to drink some water!',
      },
      trigger: {
        seconds: totalMinutes*60,
        repeats: true,
      },
    };

    Notifications.scheduleNotificationAsync({
      content: {
        title: 'Water Reminder',
        body: 'Time to drink some water!',
      },
      trigger: schedulingOptions.trigger,
    });

    console.log('Success', `Reminder set every ${hours} hours and ${minutes} minutes!`);
  };

  const handleDeleteReminder = async () => {
    await Notifications.cancelAllScheduledNotificationsAsync();

    try {
      await AsyncStorage.removeItem(waterKey);
      setNotificationInterval('');
      Alert.alert('Success', 'Reminder deleted!');
    } catch (error) {
      Alert.alert('Error', 'Failed to delete reminder info!');
    }
  };

  return(
    <View style={{justifyContent:'space-evenly',alignItems:'center',backgroundColor:'white',borderRadius:15,margin:15,padding:10}}>
        <Text>Set Time for Water Reminder</Text>
        <TimePicker value={{ hours, minutes }} onChange={handleChange} />
        <Text 
            style={{
                fontWeight:'bold',
                fontFamily:''
            }}
        >
            Get Reminder in Every : {hours} hours and : {minutes} minutes
        </Text>
        <Text 
            style={{
                fontWeight:'bold',
                fontFamily:'',
                color: 'blue'
            }}
        >
            Reminder set for every {notificationInterval===''?0:notificationInterval} minutes
        </Text>
        <TouchableOpacity
            style={{
                borderWidth: 1,
                borderColor:'#CCCCCC',
                backgroundColor: '#6fa67c',
                borderRadius:20,
                height: 50,
                paddingLeft: 20,
                paddingRight: 20,
                padding: 10,
                marginTop:5,
                width:150
            }}
            onPress={handleSetReminder}
        >
            <Text style={{
                    color: '#FFFFFF',
                    fontSize: 20,
                    textAlign: 'center',
                }}>Set
            </Text>
        </TouchableOpacity>
        <TouchableOpacity
            style={{
                borderWidth: 1,
                borderColor:'#CCCCCC',
                backgroundColor: '#6fa67c',
                borderRadius:20,
                height: 50,
                paddingLeft: 20,
                paddingRight: 20,
                padding: 10,
                marginTop:5,
                width:150
            }}
            onPress={handleDeleteReminder}
        >
            <Text style={{
                    color: '#FFFFFF',
                    fontSize: 20,
                    textAlign: 'center',
                }}>Delete
            </Text>
        </TouchableOpacity>
</View>
)
};

export default WaterReminder;
