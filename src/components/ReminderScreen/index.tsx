import React from "react";
import { View ,Text} from "react-native";
import {MaterialCommunityIcons} from '@expo/vector-icons';
import AsyncStorage from "@react-native-community/async-storage";
import TodoForm from "./ToDoForm";
import Task from "./Task";
import WaterReminder from "./WaterReminder";


/**
 * This is the main screen of Reminders , the screen starts from this function , which has -
 1.a back button and Reminder tag (this is provided by Navigation in React Native)
 2.Header , a descriptive line and add button
 3.Clicking on Add button gives a form to fill the task and time to remind it 
 4.As tasks increase the tasks are populated column wise
 * @returns a component which lets you add reminders for the day 
 */



const Reminders=()=>{

    const [todos, setTodos] = React.useState([]);//An array of reminders , has following properties - 1. activity 2. hours 3.minutes 4.ampm and 5.time
    const [isShown, setIsShown] = React.useState(false); // for showing the ToDoForm component in order to add a new activity
    const [key,setKey] =React.useState('');
    
    //add task button change on click
     const handleClick = (event:any) => {
        console.log('clicked add (+) task button');
        setIsShown(current => !current);
      };
    
    //to add activity in the list of todos or activities to be reminded of
    const addTodo = async ({activity,hours,minutes,ampm,time}:any) => {
        console.log("Came inside addTodo to add a new task to remind for");
        const newData = {
            activity : activity,
            hours : hours,
            minutes : minutes,
            ampm : ampm,
            time : time
        };
        try{
            const newTodos = [...todos, newData];
            setTodos(newTodos);
            let existingTransactions = await getData();
            const updatedTransactions = [... existingTransactions,newData];
            await AsyncStorage.setItem(
                key,
                JSON.stringify(updatedTransactions)
              );
        }
        catch (err) {
            console.log(err);
          }
    };

    //to remove activity in the list of todos or activities to be reminded of
    const removeTodo = async (index:any) => {
        console.log("Came inside removeTodo to remove a task to be reminded of")
        const newTodos = [...todos];
        newTodos.splice(index, 1);
        setTodos(newTodos);
        let existingTransactions = await getData();
            const updatedTransactions = [... existingTransactions];
            updatedTransactions.splice(index,1);
            await AsyncStorage.setItem(
                key,
                JSON.stringify(updatedTransactions)
              );
    };

    React.useEffect(()=>{
        getData();
    },[])


    //to retrieve data stored in asyncStorage for task reminders
    const getData = async () =>  {
        const res=await AsyncStorage.getItem('keyForReminder');
        if(res!=null){
            console.log('came inside new way of getting key');
            setKey(res);
        }
        const result=await AsyncStorage.getItem(res);
        console.log("key for user using result... ",res);
        console.log("key for user ... ",res);
        console.log("data retrieved successfully from async storage in getData ... ",result);
        if(result !=null){
            setTodos(JSON.parse(result));
            console.log("toDos ... ",todos);
            return JSON.parse(result);
        }
        else{
            console.log("toDos ... ",todos);
            return [];
        }
    }

    //method which renders components on the Reminders Screen 
    return(
        <View
            style={{
                flex: 1,
                backgroundColor:'#c4d7f5'
            }}
        >
            <View style=
            {{padding:20,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}
            >
                <Text style=
                {{fontSize:15,color:'#4d5054',justifyContent:'flex-start'}}
                >
                    Set reminders to organise your lifestlye
                </Text>
                <MaterialCommunityIcons name='plus' size={35}  onPress={handleClick} style=
                {{color:'#4d5054',borderRadius:20,marginHorizontal:8}} 
                />
            </View>

                {isShown && (<View
                    style={
                        {
                            backgroundColor:'white',
                            marginHorizontal:16,
                            marginVertical:4,
                            borderRadius:20,
                            paddingVertical:10,
                            paddingHorizontal:24,
                        }}
                >
                    <TodoForm addTodo={addTodo} /></View>)
                }  
                {todos!=null && todos.map((todo, index) => (
                    <Task
                            key={index}
                            index={index}
                            activity={todo.activity}
                            hours={todo.hours}
                            minutes={todo.minutes}
                            ampm={todo.ampm}
                            time={todo.time}
                            removeTodo={removeTodo}
                    />
                    ))
                }
                <WaterReminder/>
        </View>        
    )
}

export default Reminders;