import React from "react";
import { Image, StyleSheet, TextInput, View } from "react-native";
import SearchIcon from "../../assets/search.png";

const Search = ({ searchText, setSearchText }: any) => {
  const searchItem = (searchedText: any) => {
    setSearchText(searchedText);
  };
  return (
    <View style={styles.searchSection}>
      <Image style={styles.searchIcon} source={SearchIcon} />
      <TextInput
        style={styles.input}
        placeholder="Search food items to track"
        value={searchText}
        onChangeText={(searchedText) => searchItem(searchedText)}
        underlineColorAndroid="transparent"
      />
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  searchSection: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#ffffff",
    borderWidth: 2,
    borderColor: "#D9D9D9",
    borderRadius: 10,
    margin: 10,
  },
  input: {
    padding: 20,
    fontSize: 21.6,
    marginLeft: 0,
  },
  searchIcon: {
    marginLeft: 10,
  },
});
