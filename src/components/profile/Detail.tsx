import { View, Text, StyleSheet } from "react-native";
import React from "react";

const Detail = ({ detail, detailTitle }: any) => {
  return (
    <View style={styles.detailContainer}>
      <Text style={styles.detail}>{detail}</Text>
      <Text style={styles.detailTitle}>{detailTitle}</Text>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  detail: {
    fontSize: 30,
    color: "white",
    fontFamily: "sans-serif-medium",
  },
  detailContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: 11,
    paddingLeft: 18,
    paddingRight: 18,
  },
  detailTitle: {
    color: "white",
    fontFamily: "sans-serif-medium",
  },
});
