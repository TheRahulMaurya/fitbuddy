import React from "react";
import { StyleSheet, View } from "react-native";
import { Avatar, Text } from "react-native-elements";

const CustomAvatar = ({ title, name, email }: any) => {
  return (
    <View style={styles.container}>
      <Avatar
        containerStyle={styles.avatar}
        titleStyle={{ fontSize: 50 }}
        rounded
        title={title}
      />
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.email}>{email}</Text>
    </View>
  );
};

export default CustomAvatar;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    backgroundColor: "#5072A7",
    height: 100,
    width: 100,
    borderRadius: 50,
  },
  name: {
    fontSize: 30,
    marginTop: 10,
    color: "white",
    fontFamily: "sans-serif-medium",
  },
  email: {
    color: "white",
    fontFamily: "sans-serif-medium",
    fontSize: 17,
  },
});
