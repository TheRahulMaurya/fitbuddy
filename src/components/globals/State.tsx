import React from "react";
import { View, Text } from "react-native";
import { Picker } from "@react-native-picker/picker";
interface StateInputProps {
  state: string;
  setState: (value: string) => void;
  submitted: boolean;
}
const StateInput: React.FC<StateInputProps> = ({
  state,
  setState,
  submitted,
}) => {
  return (
    <View>
      <Picker
        mode="dropdown"
        selectedValue={state}
        onValueChange={(itemValue, itemIndex) => setState(itemValue)}
      >
        <Picker.Item color="grey" label="Select State" value="" />
        <Picker.Item label="Delhi" value="DELHI" />
        <Picker.Item label="Uttrakhand" value="UTTARAKHAND" />
        <Picker.Item label="Chandigarh" value="CHANDIGARH" />
        <Picker.Item label="Punjab" value="PUNJAB" />
        <Picker.Item label="Kerala" value="KERALA" />
        <Picker.Item label="Haryana" value="HARYANA" />
        <Picker.Item label="Himachal Pradesh" value="HIMACHAL_PRADESH" />
        <Picker.Item label="Jammu And Kashmir" value="JAMMU_AND_KASHMIR" />
        <Picker.Item label="Karnataka" value="KARNATAKA" />
        <Picker.Item label="Uttar Pradesh" value="UTTAR_PRADESH" />
      </Picker>
      {submitted && !state && (
        <Text style={styles.pickerError}>Please select State</Text>
      )}
    </View>
  );
};
const styles = {
  picker: {
    marginBottom: 10,
    height: 40,
    borderColor: "#ccc",
    borderWidth: 1,
  },
  errorText: {
    color: "red",
    marginBottom: 10,
  },
  pickerError: {
    color: "red",
    marginLeft: -10,
    marginBottom: 20,
  },
  errorInput: {
    borderColor: "red",
  },
};
export default StateInput;
