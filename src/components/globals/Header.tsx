import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LOGIN_SCREEN, REMINDER_SCREEN } from "../../constants/ScreenNames";
import AsyncStorage from "@react-native-community/async-storage";

interface NavigationProps {
  navigation: any;
}

const Header = ({ navigation }: NavigationProps) => {
  const reminder = () => {
    navigation.navigate(REMINDER_SCREEN);
  };

  const logout = () => {
    // AsyncStorage.clear();
    // navigation.navigate(LOGIN_SCREEN);
    AsyncStorage.removeItem('userCred',()=>{
      console.log('navigating to login screen');
      navigation.navigate(LOGIN_SCREEN);
    });
  };

  return (
    <>
      <View style={styles.header}>
        <Text style={styles.text}>Today</Text>
        <View style={styles.icons}>
          <MaterialCommunityIcons
            name="bell-outline"
            size={30}
            onPress={reminder}
          />
          <MaterialCommunityIcons
            name="logout"
            size={30}
            color="#EB6D00"
            onPress={logout}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    backgroundColor: "white",
    padding: 15,
    height: 60,
    width: 400,
    gap: 240,
    marginHorizontal: 10,
    alignSelf: "center",
  },
  text: {
    fontSize: 20,
    fontWeight: "800",
  },
  icons: {
    flexDirection: "row",
    gap: 20,
  },
});

export default Header;
