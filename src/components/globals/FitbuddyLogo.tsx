import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { Image } from "react-native-elements";

const FitbuddyLogo = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>FITBUDDY</Text>

      <View style={styles.logoContainer}>
        <Image
          source={{
            uri: "https://gitlab.com/makarandlad2/fitbuddy/uploads/b008337fd7a5dc56b1f63e6dd3f9113d/fitbuddy-logo.jpg",
          }}
          style={styles.logo}
        />
      </View>
    </View>
  );
};

export default FitbuddyLogo;

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#EB6D00",
  },
  container: {
    marginLeft: "30%",
    flexDirection: "row",
    marginTop: "20%",
    marginBottom: "5%",
    alignItems: "center",
  },
  logoContainer: {
    padding: 2,
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  logo: {
    width: 40,
    height: 40,
    borderRadius: 30,
  },
});
