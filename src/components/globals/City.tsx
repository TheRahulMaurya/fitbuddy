import React from "react";
import { View, Text } from "react-native";
import { Picker } from "@react-native-picker/picker";
interface CityInputProps {
  city: string;
  setCity: (value: string) => void;
  submitted: boolean;
}
const CityInput: React.FC<CityInputProps> = ({ city, setCity, submitted }) => {
  return (
    <View>
      <Picker
        mode="dropdown"
        selectedValue={city}
        onValueChange={(itemValue, itemIndex) => setCity(itemValue)}
      >
        <Picker.Item color="grey" label="Select City" value="" />
        <Picker.Item label="Haldwani" value="HALDWANI" />
        <Picker.Item label="Dehradun" value="DEHRADUN" />
        <Picker.Item label="Uttarkashi" value="UTTARKASHI" />
        <Picker.Item label="Chamoli" value="CHAMOLI" />
        <Picker.Item label="Almora" value="ALMORA" />
        <Picker.Item label="Amritsar" value="AMRITSAR" />
        <Picker.Item label="Ludhiana" value="LUDHIANA" />
        <Picker.Item label="Bhatinda" value="BHATINDA" />
        <Picker.Item label="Jalandar" value="JALANDHAR" />
        <Picker.Item label="Panchkula" value="PANCHKULA" />
        <Picker.Item label="Zirakpur" value="ZIRAKPUR" />
        <Picker.Item label="Gurugram" value="GURUGRAM" />
        <Picker.Item label="Ambala" value="AMBALA" />
        <Picker.Item label="Faridabad" value="FARIDABAD" />
        <Picker.Item label="Karnal" value="KARNAL" />
        <Picker.Item label="Rohtak" value="ROHTAK" />
        <Picker.Item label="Panipat" value="PANIPAT" />
        <Picker.Item label="Shimla" value="SHIMLA" />
        <Picker.Item label="Dharamshala" value="DHARAMSHALA" />
        <Picker.Item label="Kullu" value="KULLU" />
        <Picker.Item label="Chamba" value="CHAMBA" />
        <Picker.Item label="Manali" value="MANALI" />
        <Picker.Item label="Kasauli" value="KASAULI" />
        <Picker.Item label="Srinagar" value="SRINAGAR" />
        <Picker.Item label="Akhnoor" value="AKHNOOR" />
        <Picker.Item label="Kochi" value="KOCHI" />
        <Picker.Item label="Kollam" value="KOLLAM" />
        <Picker.Item label="Bengaluru" value="BENGALURU" />
        <Picker.Item label="Mysuru" value="MYSURU" />
        <Picker.Item label="Mangaluru" value="MANGALURU" />
        <Picker.Item label="Shivamogga" value="SHIVAMOGGA" />
        <Picker.Item label="Lucknow" value="LUCKNOW" />
      </Picker>
      {submitted && !city && (
        <Text style={styles.pickerError}>Please select City</Text>
      )}
    </View>
  );
};
const styles = {
  picker: {
    marginBottom: 10,
    height: 40,
    borderColor: "#ccc",
    borderWidth: 1,
  },
  errorText: {
    color: "red",
    marginBottom: 10,
  },
  pickerError: {
    color: "red",
    marginLeft: -10,
    marginBottom: 20,
  },
  errorInput: {
    borderColor: "red",
  },
};
export default CityInput;
