import { View, Text, StyleSheet } from "react-native";
import React, { useState } from "react";
import { Entypo } from "@expo/vector-icons";
import { useTailwind } from "tailwind-rn/dist";

type QuantityTogglePorps = {
  count: number;
  iconSize: number;
  width: number;
  height: number;
  style?: object;
  increament(): void;
  decreament(): void;
};

const QuantityToggle = ({
  count,
  iconSize,
  width,
  height,
  increament,
  decreament,
  style,
}: QuantityTogglePorps) => {
  const tw = useTailwind();
  return (
    <View
      style={{
        ...tw(
          "flex flex-row rounded-md justify-center items-center border border-slate-300"
        ),
        ...style,
        height: height,
        width: width,
      }}
    >
      <Entypo
        name="minus"
        size={iconSize}
        color="#EB6D00"
        onPress={decreament}
      />
      <View style={tw("w-7/12 justify-center items-center")}>
        <Text style={tw("font-medium")}>{count}</Text>
      </View>
      <Entypo
        name="plus"
        size={iconSize}
        color="#EB6D00"
        onPress={increament}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 24,
    lineHeight: 32,
    alignItems: "center",
  },
  textView: {
    width: "58.33%",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default QuantityToggle;
