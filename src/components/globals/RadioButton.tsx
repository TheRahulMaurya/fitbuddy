import { View, Text, GestureResponderEvent } from "react-native";
import React, { useState } from "react";
import { CheckBox } from "@rneui/themed";
import { useDispatch } from "react-redux";
import { setChecked } from "../../store";

type RadioButtonProps = {
  title?: string;
  isChecked?: boolean;
  foodId?: number;
};
const RadioButton = ({
  title,
  isChecked = false,
  foodId,
}: RadioButtonProps) => {
  const dispatch = useDispatch();

  const onPress = (e: any) => {
    e.preventDefault();
    dispatch(setChecked(foodId));
  };
  return (
    <View>
      <CheckBox
        title={title}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        checked={isChecked}
        onPress={onPress}
        checkedColor="black"
        uncheckedColor="black"
      ></CheckBox>
    </View>
  );
};

export default RadioButton;
