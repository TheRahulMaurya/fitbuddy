import React from "react";
import { View, Text } from "react-native";
import { Picker } from "@react-native-picker/picker";
interface CountryInputProps {
  country: string;
  setCountry: (value: string) => void;
  submitted: boolean;
}
const CountryInput: React.FC<CountryInputProps> = ({
  country,
  setCountry,
  submitted,
}) => {
  return (
    <View>
      <Picker
        mode="dropdown"
        selectedValue={country}
        onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}
      >
        <Picker.Item color="grey" label="Select Country" value="" />
        <Picker.Item label="India" value="INDIA" />
        <Picker.Item label="Nepal" value="NEPAL" />
        <Picker.Item label="USA" value="USA" />
        <Picker.Item label="UK" value="UK" />
        <Picker.Item label="Canada" value="CANADA" />
        <Picker.Item label="South Korea" value="SOUTH_KOREA" />
        <Picker.Item label="Sri Lanka" value="SRI_LANKA" />
        <Picker.Item label="Germany" value="GERMANY" />
        <Picker.Item label="France" value="FRANCE" />
      </Picker>
      {submitted && !country && (
        <Text style={styles.pickerError}>Please select Country</Text>
      )}
    </View>
  );
};
const styles = {
  picker: {
    marginBottom: 10,
    height: 40,
    borderColor: "#ccc",
    borderWidth: 1,
  },
  errorText: {
    color: "red",
    marginBottom: 10,
  },
  pickerError: {
    color: "red",
    marginLeft: -10,
    marginBottom: 20,
  },
  errorInput: {
    borderColor: "red",
  },
};
export default CountryInput;
