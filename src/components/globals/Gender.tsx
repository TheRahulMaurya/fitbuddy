import React from "react";
import { View, Text } from "react-native";
import { Picker } from "@react-native-picker/picker";
interface GenderInputProps {
  gender: string;
  setGender: (value: string) => void;
  submitted: boolean;
}
const GenderInput: React.FC<GenderInputProps> = ({
  gender,
  setGender,
  submitted,
}) => {
  return (
    <View>
      <Picker
        mode="dropdown"
        selectedValue={gender}
        onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
      >
        <Picker.Item color="grey" label="Select Gender" value="" />
        <Picker.Item label="Female" value="FEMALE" />
        <Picker.Item label="Male" value="MALE" />
        <Picker.Item label="Other" value="OTHER" />
      </Picker>
      {submitted && !gender && (
        <Text style={styles.pickerError}>Please select Gender</Text>
      )}
    </View>
  );
};
const styles = {
  picker: {
    marginBottom: 10,
    height: 40,
    borderColor: "#ccc",
    borderWidth: 1,
  },
  errorText: {
    color: "red",
    marginBottom: 10,
  },
  pickerError: {
    color: "red",
    marginLeft: -10,
    marginBottom: 20,
  },
  errorInput: {
    borderColor: "red",
  },
};
export default GenderInput;
