import React, { useRef, useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import SmoothPicker from "react-native-smooth-picker";
import { useTailwind } from "tailwind-rn/dist";

const opacities = {
  0: 1,
  1: 1,
  2: 0.6,
  3: 0.3,
  4: 0.1,
};
const sizeText = {
  0: 20,
  1: 15,
  2: 10,
};

const Item = React.memo(({ opacity, selected, vertical, fontSize, name }) => {
  return (
    <View
      style={[
        styles.OptionWrapper,
        {
          opacity,
          borderColor: "transparent",
          width: vertical ? 190 : "auto",
        },
      ]}
    >
      <Text style={{ fontSize, fontWeight: selected ? 500 : 200 }}>{name}</Text>
    </View>
  );
});

const ItemToRender = ({ item, index }, indexSelected, vertical) => {
  const selected = index === indexSelected;
  const gap = Math.abs(index - indexSelected);
  let opacity = opacities[gap];
  if (gap > 3) {
    opacity = opacities[4];
  }
  let fontSize = sizeText[gap];
  if (gap > 1) {
    fontSize = sizeText[2];
  }

  return (
    <Item
      opacity={opacity}
      selected={selected}
      vertical={vertical}
      fontSize={fontSize}
      name={item}
    />
  );
};

const HorizontalScroll = ({ propData, selectedIndex, setSelected }) => {
  function handleChange(index) {
    if (index < propData.length) {
      setSelected(index);
      refPicker.current.scrollToIndex({
        animated: false,
        index: index,
        viewOffset: -1,
      });
    }
  }

  const refPicker = useRef(null);
  const tw = useTailwind();
  return (
    <View style={tw("w-full h-12 justify-center")}>
      <SmoothPicker
        initialScrollToIndex={selectedIndex}
        refFlatList={refPicker}
        keyExtractor={(_, index) => index.toString()}
        horizontal={true}
        scrollAnimation
        showsHorizontalScrollIndicator={false}
        data={propData}
        onSelected={({ item, index }) => handleChange(index)}
        renderItem={(option) => ItemToRender(option, selectedIndex, false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    paddingBottom: 30,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  wrapperHorizontal: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    margin: "auto",
    color: "black",
  },
  wrapperVertical: {
    width: 250,
    height: 350,
    justifyContent: "center",
    alignItems: "center",
    margin: "auto",
    color: "black",
  },
  OptionWrapper: {
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    paddingLeft: 30,
    paddingRight: 30,
  },
});

export default HorizontalScroll;
