import React from "react";
import { View, ScrollView, Text } from "react-native";
import { useTailwind } from "tailwind-rn/dist";

export const Carousel = (props: any) => {
  const { items, style } = props;
  const itemsPerInterval =
    props.itemsPerInterval === undefined ? 1 : props.itemsPerInterval;

  const [intervals, setIntervals] = React.useState(1);
  const [width, setWidth] = React.useState(0);
  const tw = useTailwind();

  const init = (width: number) => {
    // initialise width
    setWidth(width);
    // initialise total intervals
    const totalItems = items.length;
    setIntervals(Math.ceil(totalItems / itemsPerInterval));
  };

  return (
    <View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        onContentSizeChange={(w, h) => init(w)}
        scrollEventThrottle={200}
        pagingEnabled
        decelerationRate="fast"
        style={tw("bg-slate-100")}
      >
        {items.map((item: any, index: number) => {
          <Text>{item.title}</Text>;
        })}
      </ScrollView>
    </View>
  );
};

export default Carousel;
