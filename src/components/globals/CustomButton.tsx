import React from "react";
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  ViewStyle,
} from "react-native";
import { useTailwind } from "tailwind-rn/dist";

type CustomButtonProps = {
  title: string;
  setWhite?: boolean;
  style?: object;
  onPress(e: GestureResponderEvent): void;
  disabled?: boolean;
};

const CustomButton = ({
  title,
  setWhite,
  style,
  onPress,
  disabled,
}: CustomButtonProps) => {
  const tw = useTailwind();
  const styles = StyleSheet.create({
    button: setWhite
      ? {
          borderColor: "orange",
          borderWidth: 3,
          backgroundColor: "white",
        }
      : disabled
      ? {
          borderColor: "#0099cc",
          backgroundColor: "#cccccc",
          color: "#666666",
        }
      : {
          backgroundColor: "#EB6D00",
        },
    buttonText: {
      color: setWhite ? "#EB6D00" : "white",
    },
  });

  return (
    <TouchableOpacity
      style={{
        ...tw("justify-center items-center h-14 w-96 rounded-lg"),
        ...style,
        ...styles.button,
      }}
      disabled={disabled}
      activeOpacity={0.6}
      onPress={onPress}
    >
      <Text style={{ ...tw(`font-bold text-2xl`), ...styles.buttonText }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

CustomButton.defaultProps = {
  setWhite: false,
};

export default CustomButton;
