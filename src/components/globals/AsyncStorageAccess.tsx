import AsyncStorage from "@react-native-community/async-storage";

const AsyncStorageAccess =  {
    
    // storing data - storing token in Async Storage with key = userCred
    async storeUserCredential (value: string)  {
        try {
        await AsyncStorage.setItem("userCred", value);
        console.log("Stored token in Async Storage.........");
        } catch (error) {
            console.log('Error in storing token in Async Storage: ', error);
        }
    }
    ,
    // getting data - retrieving token from Async Storage with key = userCred
    async getUserCred () {
        try {
            const userCred = JSON.parse(
              (await AsyncStorage.getItem("userCred")) || "{}"
            );
            const token=userCred.token;
            console.log('token in service-------------------', token);
            return token;
          } catch (error) {
            console.log('Load token error: ', error);
          }
  }

}
export default AsyncStorageAccess;