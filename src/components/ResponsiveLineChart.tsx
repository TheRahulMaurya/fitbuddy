import React, { useState } from "react";
import { View, Text } from "react-native";
import { Dimensions } from "react-native";
import { Picker } from "@react-native-picker/picker";
import { LineChart } from "react-native-chart-kit";
import { RotateInDownLeft } from "react-native-reanimated";
import {
  Chart,
  Area,
  HorizontalAxis,
  Line,
  Tooltip,
  VerticalAxis,
  ChartDataPoint,
} from "react-native-responsive-linechart";

type chartProps = {
  xAxisLabel: String;
  yAxisLabel: String;
  chartData: ChartDataPoint[];
  xMinValue: number;
  xMaxValue: number;
  yMinValue: number;
  yMaxValue: number;
};

const ResponsiveLineChart = ({
  xAxisLabel,
  yAxisLabel,
  chartData,
  xMinValue,
  xMaxValue,
  yMinValue,
  yMaxValue,
}: chartProps) => {
  return (
    <View>
      <View style={{ flexDirection: "row", marginRight: 50 }}>
        <View>
          <Text>{yAxisLabel}</Text>
        </View>
        <Chart
          style={{ height: 250, width: 400 }}
          data={chartData}
          padding={{ left: 40, bottom: 20, right: 20, top: 20 }}
          xDomain={{ min: xMinValue, max: xMaxValue }}
          yDomain={{ min: yMinValue, max: yMaxValue }}
          viewport={{ size: { width: 15 } }}
        >
          <VerticalAxis
            tickCount={10}
            theme={{
              axis: { stroke: { color: "#aaa", width: 2 } },
              ticks: { stroke: { color: "#aaa", width: 2 } },
              labels: { formatter: (v: number) => v.toFixed(0) },
            }}
          />
          <HorizontalAxis
            tickCount={31}
            theme={{
              axis: { stroke: { color: "#aaa", width: 2 } },
              ticks: { stroke: { color: "#aaa", width: 2 } },
              labels: { formatter: (v: number) => v.toFixed(0) },
            }}
          />
          <Area
            theme={{
              gradient: {
                from: { color: "#44bd32" },
                to: { color: "#44bd32", opacity: 0.2 },
              },
            }}
          />
          <Line
            theme={{
              stroke: { color: "#44bd32", width: 5 },
              scatter: {
                default: { width: 8, height: 8, rx: 4, color: "#44ad32" },
                selected: { color: "red" },
              },
            }}
            tooltipComponent={
              <Tooltip theme={{ formatter: ({ y }) => y.toFixed(0) }} />
            }
          />
        </Chart>
      </View>
      <View style={{ alignItems: "center", marginLeft: 25 }}>
        <Text>{xAxisLabel}</Text>
      </View>
    </View>
  );
};

export default ResponsiveLineChart;
