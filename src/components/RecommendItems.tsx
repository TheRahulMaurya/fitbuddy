import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { useTailwind } from "tailwind-rn/dist";
import { SELECT_ITEMS_SCREEN } from "../constants/ScreenNames";
import { selectFood, selectFoodCart, selectItems } from "../store";
import { getFoodDetail } from "../utils/FoodUtil";
import Item from "./Item";

const RecommendItems = ({ navigation, searchedText }: any) => {
  const dispatch = useDispatch();
  const selectFoodByName = (state: any) => {
    console.log("inside selectFoodByName");
    console.log(state.items.data.data);
    return {
      isLoading: state.items.isLoading,
      data: {
        data: state.items.data.data.filter((food: any) =>
          food.foodName.toLowerCase().includes(searchedText.toLowerCase())
        ),
      },
      error: state.items.error,
    };
  };
  const {
    data: items,
    isLoading,
    error,
  } = useSelector(searchedText ? selectFoodByName : selectItems);
  const tw = useTailwind();
  const foodCart = useSelector(selectFoodCart);

  function isRightIconToggle(item: any) {
    var i;
    for (i = 0; i < foodCart.length; i++) {
      if (foodCart[i] && foodCart[i].foodId === item.foodId) {
        return true;
      }
    }
    return false;
  }

  return (
    <View style={styles.recommendView}>
      {isLoading && <Text>Loading....</Text>}
      {error && <Text>{error.message}</Text>}
      <ScrollView style={styles.scroll}>
        {items ? (
          items.data.map((item: itemProps) => (
            <View key={item.foodId}>
              <Item
                foodId={item.foodId}
                image={item.foodImageUrl}
                name={item.foodName}
                detailText={getFoodDetail(item, 1, 0)}
                rightIconType={isRightIconToggle(item) ? "toggle" : "right"}
                onPress={(e) => {
                  console.log("inside onPress");
                  dispatch(selectFood(item));
                  console.log("inside onPress2");
                  navigation.navigate(SELECT_ITEMS_SCREEN);
                }}
              />
              <View style={tw("h-1 bg-gray-300")}></View>
            </View>
          ))
        ) : (
          <Text></Text>
        )}
        <View style={styles.blankSpace}></View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  recommendView: {
    width: wp("100%"),
    height: hp("100%"),
  },
  scroll: {
    flex: 1,
  },
  blankSpace: {
    marginBottom: 100,
  },
});

export default RecommendItems;
