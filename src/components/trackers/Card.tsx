import React from "react";
import { StyleSheet, Text, View, Dimensions, Image } from "react-native";

const { width } = Dimensions.get("screen");
const cardWidth = width / 2 - 20;

type tracker = {
  id?: string;
  image: string;
  name?: string;
  title: string;
  description: string;
};

type ItemProps = { trackItem: tracker };

const Card = ({ trackItem }: ItemProps) => {
  return (
    <View style={styles.conatiner}>
      <View style={styles.card}>
        <View style={{ alignItems: "center" }}>
          <Image source={{ uri: trackItem.image }} style={styles.img}></Image>
          {console.log("displaying tracker image")}
          <Text style={styles.titleText}>{trackItem.title}</Text>
          {console.log("displaying tracker title : " + trackItem.title)}
          <Text style={styles.descText}>{trackItem.description}</Text>
          {console.log("displaying tracker description")}
        </View>
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  conatiner: {
    height: 250,
  },
  card: {
    height: 200,
    width: cardWidth,
    marginHorizontal: 10,
    marginBottom: -18,
    marginTop: 33,
    borderRadius: 15,
    elevation: 5,
    backgroundColor: "white",
    alignItems: "center",
  },
  img: {
    height: 70,
    width: 70,
    margin: 10,
  },
  titleText: {
    margin: 10,
    fontSize: 20,
    fontWeight: "bold",
    fontFamily: "",
  },
  descText: {
    margin: 10,
    fontSize: 15,
    color: "#72929c",
    fontFamily: "",
  },
});
