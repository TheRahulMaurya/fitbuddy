import React from "react";
import { FlatList, StyleSheet, View, TouchableOpacity } from "react-native";

import trackerItems from "./TrackersData";
import {
  FOOD_TYPE_SCREEN,
  HOME_SCREEN,
  WATER_TRACKER_SCREEN,
  WEIGHT_TRACKER_SCREEN,
  STEPS_TRACKER_SCREEN,
} from "../../constants/ScreenNames";
import Card from "./Card";

interface NavigationProps {
  navigation: any;
}

const TrackerCard = ({ navigation }: NavigationProps) => {
  return (
    <View style={styles.conatiner}>
      {trackerItems.map((item) => (
        <TouchableOpacity
          onPress={() => {
            let screen = HOME_SCREEN;
            if (item.title === "Food Tracker") {
              console.log("Food Tracker if");
              screen = FOOD_TYPE_SCREEN;
            } else if (item.title === "Weight Tracker") {
              console.log("Weight Tracker if");
              screen = WEIGHT_TRACKER_SCREEN;
            } else if (item.title === "Steps Tracker") {
              console.log("Step Tracker if");
              screen = STEPS_TRACKER_SCREEN;
            } else if (item.title === "Water Tracker") {
              console.log("Water Tracker if");
              screen = WATER_TRACKER_SCREEN;
            }
            console.log(screen + " is clicked");
            navigation.navigate(screen);
          }}
          key={item.id}
        >
          <View>
            <Card trackItem={item} key={item.id} />
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default TrackerCard;

const styles = StyleSheet.create({
  conatiner: {
    height: 500,
    backgroundColor: "#FFFCFA",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
});
