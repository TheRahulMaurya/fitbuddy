import { AntDesign } from "@expo/vector-icons";
import React, { useState } from "react";
import {
  GestureResponderEvent,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useTailwind } from "tailwind-rn";
import { removeFood, selectFoodCart, updateQuantity } from "../store";
import QuantityToggle from "./globals/QuantityToggle";
import RadioButton from "./globals/RadioButton";

type MealProps = {
  image: string;
  name: string;
  detailText: string;
  rightIconType?: string;
  isChecked?: boolean;
  onPress?: (e: GestureResponderEvent) => void;
  foodId?: number;
};

const Item = ({
  image,
  name,
  detailText,
  rightIconType,
  isChecked,
  onPress,
  foodId,
}: MealProps) => {
  const tw = useTailwind();
  const dispatch = useDispatch();
  const foodCart = useSelector(selectFoodCart);

  const onIncreament = () => {
    let quantity = getQuantity() + 1;
    dispatch(updateQuantity({ foodId, quantity }));
  };
  const onDecreament = () => {
    var quantity = getQuantity() - 1;
    if (quantity > 0) {
      dispatch(updateQuantity({ foodId, quantity }));
    } else {
      dispatch(removeFood(foodId));
    }
  };

  function getQuantity() {
    for (var i = 0; i < foodCart.length; i++) {
      if (foodCart[i] && foodCart[i].foodId === foodId) {
        return foodCart[i].quantity;
      }
    }
    return 1;
  }

  const getRightIconType = (iconType: any) => {
    switch (iconType) {
      case "radio":
        return (
          <TouchableOpacity
            // style={{ backgroundColor: "red" }}
            onPress={() => console.log("CLicked!!")}
          >
            <RadioButton foodId={foodId} isChecked={isChecked} />
          </TouchableOpacity>
        );
      case "right":
        return (
          <AntDesign name="right" size={24} color="black" onPress={onPress} />
        );
      case "toggle":
        return (
          <View style={styles.toggleButton}>
            <QuantityToggle
              count={getQuantity()}
              iconSize={15}
              width={70}
              height={30}
              style={{ padding: 5 }}
              increament={onIncreament}
              decreament={onDecreament}
            />
          </View>
        );
      default:
        return "";
    }
  };

  return (
    <View style={tw("ml-5 mt-1 flex-row items-center")}>
      <Image
        source={{
          uri: image,
        }}
        style={tw("h-11 w-11 rounded-full")}
      />
      <View style={styles.itemDetail}>
        <Text style={tw("text-lg font-medium ")}>{name}</Text>
        <Text style={styles.detailText}>{detailText}</Text>
      </View>
      <View>{getRightIconType(rightIconType)}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  itemDetail: {
    marginLeft: 10,
    width: 288,
  },
  detailText: {
    color: "#666666",
    fontSize: 16,
    width: 255,
    flexWrap: "wrap",
  },
  toggleButton: {
    marginLeft: -30,
    marginBottom: 5,
  },
});

export default Item;
