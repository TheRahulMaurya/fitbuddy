import axios from "axios";
import {
  AUTH_SERVICE_END_POINT,
  FITBUDDY_ROOT_END_POINT,
} from "../constants/ApiConstants";

export const FitBuddyApi = axios.create({
  baseURL: FITBUDDY_ROOT_END_POINT,
  headers: {
    Accept: "application/json",
  },
});

export const AuthServiceApi = axios.create({
  baseURL: AUTH_SERVICE_END_POINT,
  headers: {
    Accept: "application/json",
  },
});
