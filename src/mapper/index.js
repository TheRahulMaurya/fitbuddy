export const mealTypeMapper = (key) => {
  switch (key) {
    case "LUNCH":
      return "Lunch";
    case "DINNER":
      return "Dinner";
    case "EVENING_SNACK":
      return "Evening Snack";
    case "BREAKFAST":
      return "Breakfast";
    default:
      return "";
  }
};

export const mealPropMapper = (key) => {
  switch (key) {
    case "MEDIUM":
      return "medium";
    case "SMALL":
      return "small";
    case "LARGE":
      return "large";
    default:
      return "";
  }
};
