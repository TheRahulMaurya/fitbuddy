import { createSlice } from "@reduxjs/toolkit";
import { ITEM } from "../../constants/SliceName";
import { fetchItems } from "../thunks/ItemThunk";

const initialState = {
  isLoading: false,
  data: null,
  error: null,
};

export const itemSlice = createSlice({
  name: ITEM,
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchItems.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(fetchItems.fulfilled, (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
      state.error = null;
    });
    builder.addCase(fetchItems.rejected, (state, action) => {
      state.isLoading = false;
      console.log(action);
      state.error = action.payload;
    });
  },
});

export const selectItems = (state) => state.items;
export const itemReducer = itemSlice.reducer;
