import { createSlice } from "@reduxjs/toolkit";
import { FOOD_CART } from "../../constants/SliceName";

export const foodCartSlice = createSlice({
  name: FOOD_CART,
  initialState: [],
  reducers: {
    addFood: (state, action) => {
      console.log("inside addFood.");
      console.log(state);
      console.log(action.payload);

      return [...state, action.payload];
    },
    removeFood: (state, action) => {
      console.log("inside removeFood.");
      let index = state.findIndex((food) => food.foodId === action.payload);
      state = state.splice(index, 1);
    },
    updateQuantity: (state, action) => {
      let index = state.findIndex(
        (food) => food.foodId === action.payload.foodId
      );
      var updateFood = state[index];
      updateFood.quantity = action.payload.quantity;
      state = [...state.slice(0, index), updateFood, ...state.slice(index + 1)];
    },
    clearCart: (state, action) => {
      console.log("Cart cleared..");
      return [];
    },
  },
});

export const selectFoodCart = (state) => state.foodCart;
export const { addFood, removeFood, updateQuantity, clearCart } =
  foodCartSlice.actions;
export const foodCartReducer = foodCartSlice.reducer;
