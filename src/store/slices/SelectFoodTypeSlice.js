import { createSlice } from "@reduxjs/toolkit";
import { SELECT_FOOD_TYPE } from "../../constants/SliceName";

export const selectFoodTypeSlice = createSlice({
  name: SELECT_FOOD_TYPE,
  initialState: null,
  reducers: {
    selectFoodType: (state, action) => {
      console.log("inside selectFoodType.");
      console.log(action.payload);
      return action.payload;
    },
  },
});

export const selectedFoodType = (state) => state.selectedFoodType;
export const { selectFoodType } = selectFoodTypeSlice.actions;
export const selectFoodTypeReducer = selectFoodTypeSlice.reducer;
