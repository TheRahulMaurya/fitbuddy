import { createSlice } from "@reduxjs/toolkit";
import { FOOD_TYPE } from "../../constants/SliceName";
import { fetchFoodType } from "../thunks/FoodTypeThunk";

const initialState = {
  isLoading: false,
  data: null,
  error: null,
};

export const foodTypeSlice = createSlice({
  name: FOOD_TYPE,
  initialState,
  reducers: {
    setChecked: (state, action) => {
      return state.data.forEach(function (part, index, types) {
        types[index].isChecked =
          types[index].mealTypeId === action.payload ? true : false;
      });
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchFoodType.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(fetchFoodType.fulfilled, (state, action) => {
      state.isLoading = false;
      state.data = action.payload.data.map((type) => {
        return { ...type, isChecked: false };
      });
      state.error = null;
    });
    builder.addCase(fetchFoodType.rejected, (state, action) => {
      state.isLoading = false;
      console.log(action);
      state.error = action.payload;
    });
  },
});

export const { setChecked, resetChecked } = foodTypeSlice.actions;
export const selectFoodTypes = (state) => state.types;
export const foodTypeReducer = foodTypeSlice.reducer;
