import { createSlice } from "@reduxjs/toolkit";
import { JWT } from "../../constants/SliceName";

export const jwtSlice = createSlice({
  name: JWT,
  initialState: null,
  reducers: {
    setToken: (state, action) => {
      console.log("inside getToken.");
      return action.payload;
    },
  },
});

export const selectJwt = (state) => state.jwt;
export const { setToken } = jwtSlice.actions;
export const jwtReducer = jwtSlice.reducer;
