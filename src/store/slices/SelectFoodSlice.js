import { createSlice } from "@reduxjs/toolkit";
import { SELECT_FOOD } from "../../constants/SliceName";

export const selectFoodSlice = createSlice({
  name: SELECT_FOOD,
  initialState: null,
  reducers: {
    selectFood: (state, action) => {
      console.log("inside selectFood.");
      return action.payload;
    },
  },
});

export const selectedFood = (state) => state.selectedFood;
export const { selectFood } = selectFoodSlice.actions;
export const selectFoodReducer = selectFoodSlice.reducer;
