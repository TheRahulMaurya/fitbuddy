import { createSlice } from "@reduxjs/toolkit";
import { FOOD_INTAKE } from "../../constants/SliceName";
import { fetchFoodIntakeForDay } from "../thunks/FoodIntakeThunk";

const initialState = {
  isLoading: false,
  data: null,
  error: null,
};

export const foodIntakeForDaySlice = createSlice({
  name: FOOD_INTAKE,
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchFoodIntakeForDay.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(fetchFoodIntakeForDay.fulfilled, (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
      state.error = null;
    });
    builder.addCase(fetchFoodIntakeForDay.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    });
  },
});

export const selectFoodIntakeForDay = (state) => state.foodIntakeForDay;
export const foodIntakeForDayReducer = foodIntakeForDaySlice.reducer;
