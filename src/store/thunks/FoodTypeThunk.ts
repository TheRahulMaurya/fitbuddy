import { createAsyncThunk } from "@reduxjs/toolkit";
import { FitBuddyApi } from "../../apis/FitBuddyApi";
import { FETCH_FOOD_TYPES_END_POINT } from "../../constants/ApiConstants";
import { FETCH_FOOD_TYPE_PREFIX } from "../../constants/ThunkPrefix";
import { getToken } from "../../utils/JwtUtil";

export const fetchFoodType = createAsyncThunk(
  FETCH_FOOD_TYPE_PREFIX,
  async () => {
    console.log("inside fetch Food types");
    const token = await getToken();
    return await FitBuddyApi.get(`${FETCH_FOOD_TYPES_END_POINT}`, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        // console.log("inside then");
        // console.log(response.data);
        return response.data;
      })
      .catch((error) => {
        console.log("inside error");
        console.log(error.response);
        console.log(error);
        return error;
      });
  }
);
