import { createAsyncThunk } from "@reduxjs/toolkit";
import { FitBuddyApi } from "../../apis/FitBuddyApi";
import { FETCH_ITEM_PREFIX } from "../../constants/ThunkPrefix";
import { getToken } from "../../utils/JwtUtil";

export const fetchItems = createAsyncThunk(
  FETCH_ITEM_PREFIX,
  async (endPointType: string) => {
    console.log("inside fetchItems: " + endPointType);
    const token = await getToken();
    return await FitBuddyApi.get(`${endPointType}`, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        // console.log("inside then");
        // console.log(response.data);
        return response.data;
      })
      .catch((error) => {
        console.log("inside error");
        console.log(error.response);
        return error;
      });
  }
);
