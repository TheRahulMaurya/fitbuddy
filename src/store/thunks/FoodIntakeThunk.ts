import { createAsyncThunk } from "@reduxjs/toolkit";
import { FitBuddyApi } from "../../apis/FitBuddyApi";
import {
  FETCH_USER_FOOD_INTAKE_FOR_DAY_END_POINT,
  FOOD_INTAKE_ROOT_END_POINT,
} from "../../constants/ApiConstants";
import {
  FETCH_FOOD_INTAKE_PREFIX,
  SAVE_FOOD_INTAKE_PREFIX,
} from "../../constants/ThunkPrefix";
import { getToken } from "../../utils/JwtUtil";

export const fetchFoodIntakeForDay = createAsyncThunk(
  FETCH_FOOD_INTAKE_PREFIX,
  async (userId: any) => {
    console.log("inside fetch Food intakes");
    const today = new Date();
    const token = await getToken();
    return await FitBuddyApi.get(
      `${FETCH_USER_FOOD_INTAKE_FOR_DAY_END_POINT + userId}`,
      {
        headers: {
          Date: `${today.getDate()}-${
            today.getMonth() + 1
          }-${today.getFullYear()}`,
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.log("inside intake error");
        console.log(error.response);
        console.log(error);
        return error;
      });
  }
);

export const saveFoodIntake = createAsyncThunk(
  SAVE_FOOD_INTAKE_PREFIX,
  async (intake: any) => {
    const token = await getToken();
    console.log("intake object");
    console.log(intake);
    return await FitBuddyApi.post(FOOD_INTAKE_ROOT_END_POINT, intake, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.log("inside intake error");
        console.log(error.response);
        console.log(error);
        return error;
      });
  }
);
