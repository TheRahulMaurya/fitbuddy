import { configureStore } from "@reduxjs/toolkit";
import { foodTypeReducer } from "./slices/FoodTypeSlice";
import { itemReducer } from "./slices/ItemSlice";
import { selectFoodReducer } from "./slices/SelectFoodSlice";
import { selectFoodTypeReducer } from "./slices/SelectFoodTypeSlice";
import { foodIntakeForDayReducer } from "./slices/FoodIntakeForDaySlice";
import { foodCartReducer } from "./slices/FoodCartSlice";
import { jwtReducer } from "./slices/JwtSlice";

export const store = configureStore({
  reducer: {
    items: itemReducer,
    types: foodTypeReducer,
    selectedFood: selectFoodReducer,
    selectedFoodType: selectFoodTypeReducer,
    foodIntakeForDay: foodIntakeForDayReducer,
    foodCart: foodCartReducer,
    jwt: jwtReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: { warnAfter: 128 },
      serializableCheck: { warnAfter: 128 },
    }),
});

export * from "./slices/ItemSlice";
export * from "./slices/SelectFoodSlice";
export * from "./slices/FoodTypeSlice";
export * from "./slices/SelectFoodTypeSlice";
export * from "./slices/FoodCartSlice";
export * from "./slices/JwtSlice";
export * from "./thunks/FoodTypeThunk";
export * from "./thunks/ItemThunk";
