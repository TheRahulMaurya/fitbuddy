import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Provider } from "react-redux";
import FoodSelectionScreen from "./src/screens/FoodSelectionScreen";
import FoodTypeScreen from "./src/screens/FoodTypeScreen";
import ForgotPasswordScreen from "./src/screens/ForgotPasswordScreen";
import GoalsScreen from "./src/screens/GoalsScreen";
import HomeScreen from "./src/screens/HomeScreen";
import LoginScreen from "./src/screens/LoginScreen";
import ProfileScreen from "./src/screens/ProfileScreen";
import RegistrationForm from "./src/screens/RegistrationForm";
import SelectItemScreen from "./src/screens/SelectItemScreen";
import SplashScreen from "./src/screens/SplashScreen";
import { store } from "./src/store";

import { TailwindProvider } from "tailwind-rn";
import FooterOption from "./src/components/globals/FooterOption";
import Header from "./src/components/globals/Header";
import CongoScreen from "./src/screens/CongoScreen";
import ReminderScreen from "./src/screens/ReminderScreen";
import SendOtpScreen from "./src/screens/SendOtpScreen";
import StepsTracker from "./src/screens/StepsTracker";
import VerifyOtpScreen from "./src/screens/VerifyOtpScreen";
import WaterTracker from "./src/screens/WaterTracker";
import WeightTracker from "./src/screens/WeightTracker";
import utilities from "./tailwind.json";
import Reminders from "./src/components/ReminderScreen";
import WeightTrackerScreen from "./src/screens/WeightTrackerScreen";
import WeightGoalScreen from "./src/screens/WeightGoalScreen";
import WeightPicker from "./src/screens/WeightPickerScreen";

export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <Provider store={store}>
      <TailwindProvider utilities={utilities}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Goals"
              component={GoalsScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="SelectItems"
              component={SelectItemScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="FoodType"
              component={FoodTypeScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="UserRegistration"
              component={RegistrationForm}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="FoodSelection"
              component={FoodSelectionScreen}
            />
            <Stack.Screen
              name="Splash"
              component={SplashScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{ title: "Sign In Here", headerShown: false }}
            />
            <Stack.Screen
              name="SendOtp"
              options={{ headerShown: false }}
              component={SendOtpScreen}
            />
            <Stack.Screen
              name="VerifyOtp"
              options={{ headerShown: false }}
              component={VerifyOtpScreen}
            />
            <Stack.Screen
              name="ForgotPassword"
              options={{ headerShown: false }}
              component={ForgotPasswordScreen}
            />
            <Stack.Screen name="Profile" component={ProfileScreen} />
            <Stack.Screen
              name="CongoScreen"
              component={CongoScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="WaterTracker"
              options={{ headerShown: false }}
              component={WaterTracker}
            />
            <Stack.Screen
              name="ReminderScreen"
              options={{ headerShown: false }}
              component={Reminders}
            />
            <Stack.Screen
              name="Footer"
              options={{ headerShown: false }}
              component={FooterOption}
            />
            {/* <Stack.Screen
              name="WeightTracker"
              options={{ headerShown: false }}
              component={WeightTracker}
            /> */}
            <Stack.Screen
              name="StepsTracker"
              options={{ headerShown: false }}
              component={StepsTracker}
            />
            <Stack.Screen
              name="Header"
              options={{ headerShown: false }}
              component={Header}
            />
            <Stack.Screen
              options={{ headerShown: false }}
              name="WeightTracker"
              component={WeightTrackerScreen}
            />
            <Stack.Screen
              options={{
                animation: "slide_from_bottom",
                headerShown: false,
                headerBackVisible: false,
              }}
              name="WeightGoals"
              component={WeightGoalScreen}
            />
            <Stack.Screen
              options={{
                animation: "slide_from_bottom",
                headerShown: false,
                headerBackVisible: false,
              }}
              name="WeightPicker"
              component={WeightPicker}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </TailwindProvider>
    </Provider>
  );
}

export const appStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  screenView: {
    width: wp("100%"),
    height: hp("100%"),
  },
});
